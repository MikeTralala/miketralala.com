# Miketralala.com

### Get started
```shell
docker-compose build --pull --no-cache
docker-compose up -d 
```

### Run back-end commands
```shell
docker-compose exec php bin/console <command>
```

### Run front-end
```shell
docker-compose run pwa
```

### Docs
- GraphQL: https://localhost/graphql
