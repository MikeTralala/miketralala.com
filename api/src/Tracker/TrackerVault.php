<?php

declare(strict_types=1);

namespace App\Tracker;

use LogicException;

final class TrackerVault implements TrackerVaultInterface
{
    private ?string $trackingId = null;

    public function getTrackingId(): ?string
    {
        return $this->trackingId;
    }

    public function setTrackerId(string $trackingId): void
    {
        if ($this->trackingId !== null) {
            throw new LogicException('Tracking id is already set');
        }

        $this->trackingId = $trackingId;
    }

    public function overrideTrackerId(string $trackingId): void
    {
        $this->trackingId = $trackingId;
    }
}
