<?php

declare(strict_types=1);

namespace App\Message\Event\Handler;

use App\Message\Command\SendActivationEmail;
use App\Message\Event\UserRegistered;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendActivationMailHandler implements EventHandlerInterface
{
    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    public function __invoke(UserRegistered $event): void
    {
        $this->messageBus->dispatch(new SendActivationEmail($event->userId));
    }
}
