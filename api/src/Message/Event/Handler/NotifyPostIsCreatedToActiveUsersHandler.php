<?php

declare(strict_types=1);

namespace App\Message\Event\Handler;

use App\Message\Command\NotifyUserAboutNewPost;
use App\Message\Event\PostCreated;
use App\Repository\BlogPostRepository;
use App\Repository\UserRepository;
use DateInterval;
use Symfony\Component\Messenger\Handler\MessageSubscriberInterface;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;

final class NotifyPostIsCreatedToActiveUsersHandler implements MessageSubscriberInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private BlogPostRepository $blogPostRepository,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(PostCreated $event): void
    {
        $post  = $this->blogPostRepository->mustFind($event->postId);
        $users = $this->userRepository->findActive();

        foreach ($users as $user) {
            if ($user->getId() === $post->getAuthor()->getId()) {
                continue;
            }

            $this->messageBus->dispatch(new NotifyUserAboutNewPost($user->getId(), $post->getId()), [
                DelayStamp::delayFor(DateInterval::createFromDateString('10 seconds')),
//                DelayStamp::delayFor(DateInterval::createFromDateString('1 day')),
            ]);
        }
    }

    public static function getHandledMessages(): iterable
    {
        yield PostCreated::class => [
            'from_transport' => 'async',
        ];
    }
}
