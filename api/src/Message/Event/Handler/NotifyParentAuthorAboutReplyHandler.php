<?php

declare(strict_types=1);

namespace App\Message\Event\Handler;

use App\Message\Command\NotifyUserAboutReply;
use App\Message\Event\ReplyAdded;
use App\Repository\BlogPostReplyRepository;
use DateInterval;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DelayStamp;

final class NotifyParentAuthorAboutReplyHandler implements EventHandlerInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private BlogPostReplyRepository $blogPostReplyRepository
    ) {
    }

    public function __invoke(ReplyAdded $event): void
    {
        $reply = $this->blogPostReplyRepository->mustFind($event->replyId);

        $authorId = $reply->getParentReply()?->getAuthor()?->getId();
        if (null === $authorId) {
            $authorId = $reply->getPost()?->getAuthor()?->getId();
        }

        $this->messageBus->dispatch(new NotifyUserAboutReply($authorId, $reply->getId()), [
            DelayStamp::delayFor(DateInterval::createFromDateString('1 day')),
        ]);
    }
}
