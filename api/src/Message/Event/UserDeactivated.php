<?php

declare(strict_types=1);

namespace App\Message\Event;

use Symfony\Component\Validator\Constraints as Assert;

final class UserDeactivated
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId
    ) {
    }
}
