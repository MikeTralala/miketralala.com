<?php

declare(strict_types=1);

namespace App\Message\Command;

final class Unlike
{
    public function __construct(
        public readonly string $userId,
        public readonly ?string $postId,
        public readonly ?string $replyId,
    ) {
    }
}
