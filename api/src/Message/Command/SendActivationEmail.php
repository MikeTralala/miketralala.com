<?php

declare(strict_types=1);

namespace App\Message\Command;

use App\Message\AsynchronousMessageInterface;

final class SendActivationEmail implements AsynchronousMessageInterface
{
    public function __construct(
        public readonly string $userId
    ) {
    }
}
