<?php

declare(strict_types=1);

namespace App\Message\Command;

use App\Message\AsynchronousMessageInterface;
use Symfony\Component\Validator\Constraints as Assert;

final class NotifyUserAboutReply implements AsynchronousMessageInterface
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $userId,
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $replyId
    ) {
    }
}
