<?php

declare(strict_types=1);

namespace App\Message\Command;

use Symfony\Component\Validator\Constraints as Assert;

final class AddPost
{
    public function __construct(
        #[Assert\Ulid]
        #[Assert\NotBlank]
        public readonly string $id,
        #[Assert\NotBlank]
        public readonly string $title,
        #[Assert\NotBlank]
        public readonly string $content,
        #[Assert\NotBlank]
        public readonly string $image,
        #[Assert\NotBlank]
        public readonly string $authorId
    ) {
    }
}
