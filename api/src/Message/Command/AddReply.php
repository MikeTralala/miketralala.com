<?php

declare(strict_types=1);

namespace App\Message\Command;

final class AddReply
{
    public function __construct(
        public readonly string $content,
        public readonly string $authorId,
        public readonly ?string $postId,
        public readonly ?string $replyId,
    ) {
    }
}
