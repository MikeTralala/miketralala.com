<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Exception\AppRuntimeException;
use App\Message\Command\ActivateUser;
use App\Message\Event\UserActivated;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class ActivateUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageBusInterface $eventBus
    ) {
    }

    public function __invoke(ActivateUser $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        if ($user->isActive()) {
            throw new AppRuntimeException('User is already active');
        }

        $user->activate();

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserActivated($user->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
