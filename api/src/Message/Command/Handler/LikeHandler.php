<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Entity\BlogPostLike;
use App\Entity\Likeable;
use App\Exception\AppRuntimeException;
use App\Helper\MessageHandlerHelper;
use App\Message\Command\Like;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;

final class LikeHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageHandlerHelper $helper
    ) {
    }

    public function __invoke(Like $command): void
    {
        /** @var Likeable $parent */
        $repository = $this->helper->getRepository($command);
        $parent     = $repository->find($this->helper->getId($command));

        if (null === $parent) {
            throw new AppRuntimeException('Parent was not found');
        }

        $user = $this->userRepository->mustFind($command->userId);

        $like = $parent->getLike(
            Criteria::create()
                    ->where(Criteria::expr()?->eq('by', $user))
        );

        if (null !== $like) {
            throw AppRuntimeException::alreadyExists(BlogPostLike::class, 'by', $user->getId());
        }

        $parent->addLike(
            like: new BlogPostLike($user)
        );

        $repository->save($parent);
    }
}
