<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\Command\RemoveReply;
use App\Repository\BlogPostReplyRepository;
use App\Repository\BlogPostRepository;

final class RemoveReplyHandler implements CommandHandlerInterface
{
    public function __construct(
        private BlogPostRepository $blogPostRepository,
        private BlogPostReplyRepository $blogPostReplyRepository
    ) {
    }

    public function __invoke(RemoveReply $command): void
    {
        $reply = $this->blogPostReplyRepository->mustFind($command->replyId);
        $post  = $reply->getPost();

        if (null !== $post) {
            $post->removeReply($reply);

            $this->blogPostRepository->save($post);

            return;
        }

        $parent = $reply->getParentReply();
        if (null !== $parent) {
            $parent->removeReply($reply);

            $this->blogPostReplyRepository->save($parent);
        }
    }
}
