<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Entity\BlogPostReply;
use App\Helper\MessageHandlerHelper;
use App\Message\Command\AddReply;
use App\Message\Event\ReplyAdded;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class AddReplyHandler implements CommandHandlerInterface
{
    public function __construct(
        private MessageBusInterface $eventBus,
        private UserRepository $userRepository,
        private MessageHandlerHelper $helper
    ) {
    }

    public function __invoke(AddReply $command): void
    {
        $repository = $this->helper->getRepository($command);
        $parent     = $repository->mustFind($this->helper->getId($command));

        $user = $this->userRepository->mustFind($command->authorId);

        $reply = new BlogPostReply(
            author: $user,
            content: $command->content
        );

        $parent->addReply($reply);

        $repository->save($parent);

        $this->eventBus->dispatch(new ReplyAdded($reply->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
