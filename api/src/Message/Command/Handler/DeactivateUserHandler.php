<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Exception\AppRuntimeException;
use App\Message\Command\DeactivateUser;
use App\Message\Event\UserDeactivated;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class DeactivateUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageBusInterface $eventBus
    ) {
    }

    public function __invoke(DeactivateUser $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        if (! $user->isActive()) {
            throw new AppRuntimeException('User is already inactive');
        }

        $user->deactivate();

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserDeactivated($user->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
