<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Entity\BlogPostState;
use App\Message\Command\ChangePostState;
use App\Repository\BlogPostRepository;
use App\Repository\BlogPostStatusRepository;
use App\Repository\UserRepository;

final class ChangePostStateHandler implements CommandHandlerInterface
{
    public function __construct(
        private BlogPostRepository $blogPostRepository,
        private BlogPostStatusRepository $blogPostStatusRepository,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(ChangePostState $command): void
    {
        $post = $this->blogPostRepository->mustFind($command->postId);

        $post->addState(
            new BlogPostState(
                $this->blogPostStatusRepository->find($command->statusId),
                $this->userRepository->mustFind($command->userId)
            )
        );

        $this->blogPostRepository->save($post);
    }
}
