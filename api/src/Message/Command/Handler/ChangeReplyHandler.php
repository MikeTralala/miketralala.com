<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\Command\ChangeReply;
use App\Repository\BlogPostReplyRepository;

final class ChangeReplyHandler implements CommandHandlerInterface
{
    public function __construct(
        private BlogPostReplyRepository $blogPostReplyRepository
    ) {
    }

    public function __invoke(ChangeReply $command): void
    {
        $reply = $this->blogPostReplyRepository->mustFind($command->replyId);

        $reply->setContent($command->content);

        $this->blogPostReplyRepository->save($reply);
    }
}
