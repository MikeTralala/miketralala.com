<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Entity\Role;
use App\Entity\User;
use App\Exception\AppRuntimeException;
use App\Message\Command\RegisterUser;
use App\Message\Event\UserRegistered;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class RegisterUserHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private UserPasswordHasherInterface $passwordHasher,
        private MessageBusInterface $eventBus
    ) {
    }

    public function __invoke(RegisterUser $command): void
    {
        $user = $this->userRepository->findByEmail($command->email);
        if (null !== $user) {
            throw AppRuntimeException::alreadyExists(User::class, 'email', $command->email);
        }

        $roles = [];

        if ($command->isAuthor) {
            $roles = [Role::AUTHOR->name()];
        }

        $user = new User(
            $command->email,
            $command->name,
            $roles
        );

        $user->setPassword(
            $this->passwordHasher->hashPassword($user, $command->password)
        );

        $this->userRepository->save($user);

        $this->eventBus->dispatch(new UserRegistered($user->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }
}
