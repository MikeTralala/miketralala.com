<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\Command\SendActivationEmail;
use App\Message\Command\SendEmail;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Symfony\Component\Messenger\MessageBusInterface;

final class SendActivationEmailHandler implements CommandHandlerInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(SendActivationEmail $command): void
    {
        $user = $this->userRepository->mustFind($command->userId);

        if ($user->isActive()) {
            throw new UnrecoverableMessageHandlingException('User is already active');
        }

        $this->messageBus->dispatch(
            new SendEmail(
                $user->getEmail(),
                'account.activation.email-subject',
                'account.activation.email-content'
            )
        );
    }
}
