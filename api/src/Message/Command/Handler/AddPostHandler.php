<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Entity\BlogPost;
use App\Entity\BlogPostState;
use App\Entity\BlogPostStatus;
use App\Exception\AppRuntimeException;
use App\Helper\Utilities;
use App\Message\Command\AddPost;
use App\Message\Event\PostCreated;
use App\Repository\BlogPostRepository;
use App\Repository\BlogPostStatusRepository;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Messenger\Stamp\DispatchAfterCurrentBusStamp;

final class AddPostHandler implements CommandHandlerInterface
{
    public function __construct(
        private MessageBusInterface $eventBus,
        private BlogPostRepository $blogPostRepository,
        private BlogPostStatusRepository $blogPostStatusRepository,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke(AddPost $command): void
    {
        $slug = $this->createUniqueSlug($command->title);

        $post = new BlogPost(
            $command->id,
            $command->title,
            $slug,
            $command->content,
            $command->image,
            new BlogPostState(
                $this->blogPostStatusRepository->find(BlogPostStatus::DRAFT),
                $this->userRepository->find($command->authorId)
            )
        );

        $this->blogPostRepository->save($post);

        $this->eventBus->dispatch(new PostCreated($post->getId()), [
            new DispatchAfterCurrentBusStamp(),
        ]);
    }

    private function createUniqueSlug(string $title): string
    {
        for ($i = 0; $i < 100; $i++) {
            $slug = Utilities::slugify(trim($title));

            if ($i > 0) {
                $slug .= "-{$i}";
            }

            if (null === $this->blogPostRepository->findBySlug($slug)) {
                return $slug;
            }
        }

        throw AppRuntimeException::alreadyExists(BlogPost::class, 'slug', $slug);
    }
}
