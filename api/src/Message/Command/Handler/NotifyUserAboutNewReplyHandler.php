<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\Command\NotifyUserAboutReply;
use App\Message\Command\SendEmail;
use App\Repository\BlogPostReplyRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Symfony\Component\Messenger\MessageBusInterface;

final class NotifyUserAboutNewReplyHandler
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserRepository $userRepository,
        private BlogPostReplyRepository $blogPostReplyRepository
    ) {
    }

    public function __invoke(NotifyUserAboutReply $command): void
    {
        $user  = $this->userRepository->mustFind($command->userId);
        $reply = $this->blogPostReplyRepository->mustFind($command->replyId);

        $userReplies = $reply->getReplies(
            Criteria::create()
                    ->where(Criteria::expr()?->eq('author', $user))
        );

        if (! $userReplies->isEmpty()) {
            throw new UnrecoverableMessageHandlingException(sprintf('User "%s" already replied', $user->getId()));
        }

        $this->messageBus->dispatch(
            new SendEmail(
                $user->getEmail(),
                "There is a new reply",
                "{$user->getName()} has replied to your message, view it <link>"
            )
        );
    }
}
