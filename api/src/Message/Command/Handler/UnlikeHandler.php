<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Entity\BlogPostLike;
use App\Entity\Likeable;
use App\Exception\AppRuntimeException;
use App\Helper\MessageHandlerHelper;
use App\Message\Command\Unlike;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;

final class UnlikeHandler implements CommandHandlerInterface
{
    public function __construct(
        private UserRepository $userRepository,
        private MessageHandlerHelper $helper
    ) {
    }

    public function __invoke(Unlike $command): void
    {
        /** @var Likeable $subject */
        $repository = $this->helper->getRepository($command);
        $subject    = $repository->find($this->helper->getId($command));
        if (null === $subject) {
            throw new AppRuntimeException('Parent was not found');
        }

        $user = $this->userRepository->mustFind($command->userId);

        $likeToUnlike = $subject->getLike(
            Criteria::create()
                    ->where(Criteria::expr()?->eq('by', $user))
                    ->orderBy(['at' => Criteria::DESC])
        );

        if (null === $likeToUnlike) {
            throw AppRuntimeException::notFound(BlogPostLike::class, $user->getId(), 'by');
        }

        $subject->removeLike($likeToUnlike);

        $repository->save($subject);
    }
}
