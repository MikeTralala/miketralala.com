<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\Command\SendEmail;

final class SendEmailHandler implements CommandHandlerInterface
{
    public function __invoke(SendEmail $command): void
    {
        // TODO: write mailer implementation
    }
}
