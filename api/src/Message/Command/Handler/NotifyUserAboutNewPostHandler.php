<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\AsynchronousMessageInterface;
use App\Message\Command\NotifyUserAboutNewPost;
use App\Message\Command\SendEmail;
use App\Repository\BlogPostRepository;
use App\Repository\UserRepository;
use Doctrine\Common\Collections\Criteria;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Exception\UnrecoverableMessageHandlingException;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Messenger\MessageBusInterface;

final class NotifyUserAboutNewPostHandler implements MessageHandlerInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserRepository $userRepository,
        private BlogPostRepository $blogPostRepository,
        private LoggerInterface $messageLogger
    ) {
    }

    public function __invoke(NotifyUserAboutNewPost $command): void
    {
        $this->messageLogger->info('Message handled not');

        $user = $this->userRepository->mustFind($command->userId);
        $post = $this->blogPostRepository->mustFind($command->postId);

        $userReplies = $post->getReplies(
            Criteria::create()
                    ->where(Criteria::expr()?->eq('author', $user))
        );

        if (! $userReplies->isEmpty()) {
            throw new UnrecoverableMessageHandlingException(sprintf('User "%s" already replied', $user->getId()));
        }

        $this->messageBus->dispatch(
            new SendEmail($user->getEmail(), $post->getTitle(), 'A new blog post is here for you, you can read it <link>')
        );
    }
}
