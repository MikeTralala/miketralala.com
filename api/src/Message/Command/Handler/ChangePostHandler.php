<?php

declare(strict_types=1);

namespace App\Message\Command\Handler;

use App\Message\Command\ChangePost;
use App\Repository\BlogPostRepository;

final class ChangePostHandler implements CommandHandlerInterface
{
    public function __construct(
        private BlogPostRepository $blogPostRepository
    ) {
    }

    public function __invoke(ChangePost $command): void
    {
        $post = $this->blogPostRepository->mustFind($command->postId);

        $post->setTitle($command->title);
        $post->setContent($command->content);
        $post->setImage($command->image);

        $this->blogPostRepository->save($post);
    }
}
