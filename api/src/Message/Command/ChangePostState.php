<?php

declare(strict_types=1);

namespace App\Message\Command;

final class ChangePostState
{
    public function __construct(
        public readonly string $postId,
        public readonly int $statusId,
        public readonly string $userId
    ) {
    }
}
