<?php

declare(strict_types=1);

namespace App\Message\Command;

final class ChangePost
{
    public function __construct(
        public readonly string $postId,
        public readonly string $title,
        public readonly string $content,
        public readonly string $image,
        public readonly string $authorId
    ) {
    }
}
