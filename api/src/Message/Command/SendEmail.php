<?php

declare(strict_types=1);

namespace App\Message\Command;

use App\Message\AsynchronousMessageInterface;

final class SendEmail implements AsynchronousMessageInterface
{
    public const DEFAULT_FROM = 'noreply@miketralala.com';

    public function __construct(
        public readonly string $recipients,
        public readonly string $subject,
        public readonly string $content,
        public readonly string $from = self::DEFAULT_FROM
    ) {
    }
}
