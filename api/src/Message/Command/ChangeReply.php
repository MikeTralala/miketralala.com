<?php

declare(strict_types=1);

namespace App\Message\Command;

final class ChangeReply
{
    public function __construct(
        public readonly string $replyId,
        public readonly string $content
    )
    {
    }
}
