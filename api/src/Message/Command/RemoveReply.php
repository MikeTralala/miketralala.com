<?php

declare(strict_types=1);

namespace App\Message\Command;

final class RemoveReply
{
    public function __construct(
        public readonly string $replyId
    ) {
    }
}
