<?php

declare(strict_types=1);

namespace App\Messenger\Middleware;

use App\Messenger\Stamp\TrackerStamp;
use App\Tracker\TrackerVaultInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;

final class TrackerMiddleware implements MiddlewareInterface
{
    public function __construct(private TrackerVaultInterface $vault) { }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        if (null === $envelope->last(TrackerStamp::class)) {
            $envelope = $envelope->with(
                new TrackerStamp($this->vault->getTrackingId())
            );
        }

        return $stack->next()->handle($envelope, $stack);
    }
}
