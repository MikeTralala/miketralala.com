<?php

declare(strict_types=1);

namespace App\Messenger\Middleware;

use App\Helper\Utilities;
use Psr\Log\LoggerInterface;
use Symfony\Component\Messenger\Envelope;
use Symfony\Component\Messenger\Middleware\MiddlewareInterface;
use Symfony\Component\Messenger\Middleware\StackInterface;
use Symfony\Component\Messenger\Stamp\BusNameStamp;
use Symfony\Component\Messenger\Stamp\DelayStamp;
use Symfony\Component\Messenger\Stamp\HandledStamp;
use Symfony\Component\Messenger\Stamp\ReceivedStamp;

final class MonitoringMiddleware implements MiddlewareInterface
{
    public function __construct(
        private LoggerInterface $monitoringLogger
    ) {
    }

    public function handle(Envelope $envelope, StackInterface $stack): Envelope
    {
        $startedAt = microtime(true);
        $envelope  = $stack->next()->handle($envelope, $stack);
        $doneAt    = microtime(true);

        if (null === $envelope->last(HandledStamp::class)) {
            return $envelope;
        }

        $this->monitoringLogger->info('Message captured', [
            'name'     => Utilities::getClassShortName(get_class($envelope->getMessage())),
            'time'     => round($doneAt - $startedAt, 4),
            'memory'   => memory_get_usage(),
            'category' => $envelope->last(BusNameStamp::class)?->getBusName(),
            'message'  => $envelope->getMessage(),
            'async'    => null !== $envelope->last(ReceivedStamp::class),
            'delay'    => $envelope->last(DelayStamp::class)?->getDelay() ?? 0,
        ]);

        return $envelope;
    }
}
