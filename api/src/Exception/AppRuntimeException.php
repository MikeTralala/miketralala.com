<?php /** @noinspection AllyPlainPhpInspection */

declare(strict_types=1);

namespace App\Exception;

use App\Helper\Utilities;
use RuntimeException;

final class AppRuntimeException extends RuntimeException
{
    public static function notFound(string $className, string $value = '', string $identifierName = 'id'): self
    {
        if (empty($value)) {
            return new self(sprintf('"%s" was not found', Utilities::getClassShortName($className)));
        }

        return new self(
            sprintf(
                '%s was not found with %s: "%s"',
                Utilities::getClassShortName($className),
                $identifierName,
                $value
            )
        );
    }

    public static function alreadyExists(string $className, string $identifierName, string $value): self
    {
        return new self(
            sprintf(
                '"%s" already exists with %s: "%s"',
                Utilities::getClassShortName($className),
                $identifierName,
                $value
            )
        );
    }
}
