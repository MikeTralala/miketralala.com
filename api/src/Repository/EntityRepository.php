<?php

declare(strict_types=1);

namespace App\Repository;

use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\UnitOfWork;
use RuntimeException;

abstract class EntityRepository extends ServiceEntityRepository
{
    public function save(object $entity): void
    {
        if (! $entity instanceof $this->_entityName) {
            throw new RuntimeException(sprintf('The entity "%s" can not be saved by "%s"', get_class($entity), $this::class));
        }

        $unitOfWork = $this->getEntityManager()->getUnitOfWork();
        if (UnitOfWork::STATE_NEW === $unitOfWork->getEntityState($entity, UnitOfWork::STATE_NEW)) {
            $this->getEntityManager()->persist($entity);
        }

        $this->getEntityManager()->flush($entity);
    }

    public function remove(object $entity): void
    {
        if (! $entity instanceof $this->_entityName) {
            throw new RuntimeException(sprintf('The entity "%s" can not be saved by "%s"', get_class($entity), $this::class));
        }

        $this->getEntityManager()->remove($entity);
        $this->getEntityManager()->flush($entity);
    }
}
