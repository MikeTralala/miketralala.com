<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\BlogPostStatus;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogPostStatus|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPostStatus|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPostStatus[]    findAll()
 * @method BlogPostStatus[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostStatusRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: BlogPostStatus::class
        );
    }
}
