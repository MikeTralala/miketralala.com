<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\BlogPostReply;
use App\Exception\AppRuntimeException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogPostReply|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPostReply|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPostReply[]    findAll()
 * @method BlogPostReply[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostReplyRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: BlogPostReply::class
        );
    }

    public function mustFind(string $id): BlogPostReply
    {
        $reply = $this->find($id);
        if (null === $reply) {
            throw AppRuntimeException::notFound(BlogPostReply::class, $id);
        }

        return $reply;
    }
}
