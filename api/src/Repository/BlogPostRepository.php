<?php

declare(strict_types=1);

namespace App\Repository;

use App\Entity\BlogPost;
use App\Exception\AppRuntimeException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method BlogPost|null find($id, $lockMode = null, $lockVersion = null)
 * @method BlogPost|null findOneBy(array $criteria, array $orderBy = null)
 * @method BlogPost[]    findAll()
 * @method BlogPost[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BlogPostRepository extends EntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct(
            registry: $registry,
            entityClass: BlogPost::class
        );
    }

    public function mustFind(string $id): BlogPost
    {
        $post = $this->find($id);
        if (null === $post) {
            throw AppRuntimeException::notFound(BlogPost::class, $id);
        }

        return $post;
    }

    public function findBySlug(string $slug): ?BlogPost
    {
        $query = $this->createQueryBuilder('post')
                      ->where('post.slug = :slug')
                      ->getQuery();

        return $query
            ->setParameter('slug', $slug)
            ->setMaxResults(1)
            ->getOneOrNullResult();
    }
}
