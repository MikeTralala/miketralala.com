<?php

declare(strict_types=1);

namespace App\Helper;

use App\Repository\BlogPostReplyRepository;
use App\Repository\BlogPostRepository;
use RuntimeException;

final class MessageHandlerHelper
{
    public function __construct(
        private BlogPostRepository $blogPostRepository,
        private BlogPostReplyRepository $blogPostReplyRepository
    ) {
    }

    public function getRepository(object $message): BlogPostRepository|BlogPostReplyRepository
    {
        if (null !== $message->postId) {
            return $this->blogPostRepository;
        }

        if (null !== $message->replyId) {
            return $this->blogPostReplyRepository;
        }

        throw new RuntimeException('Unable to determine what repository to use');
    }

    public function getId(object $message): string
    {
        if (null === $message->postId && null === $message->replyId) {
            throw new RuntimeException('Both postId and replyId are empty');
        }

        return $message->postId ?? $message->replyId;
    }
}
