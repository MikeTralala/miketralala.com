<?php

declare(strict_types=1);

namespace App\Helper;

use ReflectionClass;

final class Utilities
{
    public static function getClassShortName(string $className): string
    {
        return (new ReflectionClass($className))->getShortName();
    }

    public static function slugify(string $string): string
    {
        return strtolower(
            trim(
                preg_replace(
                    '/[^A-Za-z0-9-]+/',
                    '-',
                    trim($string)
                )
            )
        );
    }
}
