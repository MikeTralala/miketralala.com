<?php

declare(strict_types=1);

namespace App\Helper;

use App\Entity\User;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class UserHelper
{
    public function __construct(
        private TokenStorageInterface $tokenStorage
    ) {
    }

    public function getUser(): ?User
    {
        $user = $this->tokenStorage->getToken()?->getUser();

        if (! $user instanceof User) {
            return null;
        }

        return $user;
    }
}
