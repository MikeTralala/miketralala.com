<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\BlogPostReply;
use App\Message\Command\ChangeReply;
use RuntimeException;
use Symfony\Component\Messenger\MessageBusInterface;

class ChangeBlogPostReplyResolver implements MutationResolverInterface
{

    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    public function __invoke($item, array $context): BlogPostReply
    {
        if (! $item instanceof BlogPostReply) {
            throw new RuntimeException('BlogPostReply was not found');
        }

        $this->messageBus->dispatch(
            new ChangeReply(
                $item->getId(),
                $context['args']['input']['content']
            )
        );

        return $item;
    }
}
