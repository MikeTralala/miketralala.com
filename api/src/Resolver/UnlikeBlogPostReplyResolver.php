<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\BlogPostReply;
use App\Helper\UserHelper;
use App\Message\Command\Unlike;
use RuntimeException;
use Symfony\Component\Messenger\MessageBusInterface;

final class UnlikeBlogPostReplyResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserHelper $userHelper
    ) {
    }

    public function __invoke($item, array $context): BlogPostReply
    {
        if (! $item instanceof BlogPostReply) {
            throw new RuntimeException('BlogPostReply was not found');
        }

        $this->messageBus->dispatch(
            new Unlike(
                $this->userHelper->getUser()?->getId(),
                postId: null,
                replyId: $item->getId()
            )
        );

        return $item;
    }
}
