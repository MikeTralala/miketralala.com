<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\BlogPost;
use App\Helper\UserHelper;
use App\Message\Command\Like;
use RuntimeException;
use Symfony\Component\Messenger\MessageBusInterface;

final class LikeBlogPostResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserHelper $userHelper
    ) {
    }

    public function __invoke($item, array $context): BlogPost
    {
        if (! $item instanceof BlogPost) {
            throw new RuntimeException('BlogPost was not found');
        }

        $this->messageBus->dispatch(
            new Like(
                $this->userHelper->getUser()?->getId(),
                postId: $item->getId(),
                replyId: null
            )
        );

        return $item;
    }
}
