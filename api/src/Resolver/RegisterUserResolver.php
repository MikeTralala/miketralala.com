<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\User;
use App\Message\Command\RegisterUser;
use App\Repository\UserRepository;
use Symfony\Component\Messenger\MessageBusInterface;

final class RegisterUserResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserRepository $userRepository
    ) {
    }

    public function __invoke($item, array $context): User
    {
        $this->messageBus->dispatch(
            new RegisterUser(
                $context['args']['input']['email'],
                $context['args']['input']['name'],
                $context['args']['input']['password'],
                $context['args']['input']['isAuthor'],
            )
        );

        return $this->userRepository->findByEmail($context['args']['input']['email']);
    }
}
