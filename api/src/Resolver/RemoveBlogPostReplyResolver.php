<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Message\Command\RemoveReply;
use Symfony\Component\Messenger\MessageBusInterface;

final class RemoveBlogPostReplyResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus
    ) {
    }

    public function __invoke($item, array $context): void
    {
        $this->messageBus->dispatch(
            new RemoveReply(
                $item->getId()
            )
        );
    }
}
