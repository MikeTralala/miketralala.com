<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\User;
use App\Exception\AppRuntimeException;
use App\Message\Command\ActivateUser;
use Symfony\Component\Messenger\MessageBusInterface;

class ActivateUserResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
    ) {
    }

    public function __invoke($item, array $context): User
    {
        if (! $item instanceof User) {
            throw AppRuntimeException::notFound(User::class);
        }

        $this->messageBus->dispatch(
            new ActivateUser(
                $item->getId()
            )
        );

        return $item;
    }
}
