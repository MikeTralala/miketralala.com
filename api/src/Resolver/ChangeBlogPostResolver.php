<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\BlogPost;
use App\Helper\UserHelper;
use App\Message\Command\ChangePost;
use RuntimeException;
use Symfony\Component\Messenger\MessageBusInterface;

final class ChangeBlogPostResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserHelper $userHelper
    ) {
    }

    public function __invoke($item, array $context): BlogPost
    {
        if (! $item instanceof BlogPost) {
            throw new RuntimeException('BlogPost was not found');
        }

        $this->messageBus->dispatch(
            new ChangePost(
                $item->getId(),
                $context['args']['input']['title'],
                $context['args']['input']['content'],
                $context['args']['input']['image'],
                $this->userHelper->getUser()?->getId()
            )
        );

        return $item;
    }
}
