<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\BlogPost;
use App\Helper\UserHelper;
use App\Message\Command\AddPost;
use App\Repository\BlogPostRepository;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Uid\Ulid;

final class AddBlogPostResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserHelper $userHelper,
        private BlogPostRepository $blogPostRepository
    ) {
    }

    public function __invoke($item, array $context): BlogPost
    {
        $id = Ulid::generate();

        $this->messageBus->dispatch(
            new AddPost(
                $id,
                $context['args']['input']['title'],
                $context['args']['input']['content'],
                $context['args']['input']['image'],
                $this->userHelper->getUser()?->getId()
            )
        );

        return $this->blogPostRepository->mustFind($id);
    }
}
