<?php

declare(strict_types=1);

namespace App\Resolver;

use ApiPlatform\Core\GraphQl\Resolver\MutationResolverInterface;
use App\Entity\BlogPost;
use App\Exception\AppRuntimeException;
use App\Helper\UserHelper;
use App\Message\Command\AddReply;
use Symfony\Component\Messenger\MessageBusInterface;

final class AddBlogPostReplyToPostResolver implements MutationResolverInterface
{
    public function __construct(
        private MessageBusInterface $messageBus,
        private UserHelper $userHelper
    ) {
    }

    public function __invoke($item, array $context): BlogPost
    {
        if (! $item instanceof BlogPost) {
            throw AppRuntimeException::notFound(BlogPost::class);
        }

        $this->messageBus->dispatch(
            new AddReply(
                $context['args']['input']['content'],
                $this->userHelper->getUser()?->getId(),
                postId: $item->getId(),
                replyId: null
            )
        );

        return $item;
    }
}
