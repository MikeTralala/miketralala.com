<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;

final class MonitoringSubscriber implements EventSubscriberInterface
{
    private float $startedAt = 0.0;

    public function __construct(
        private LoggerInterface $monitoringLogger,
        private TokenStorageInterface $tokenStorage
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class  => ['start', 99999],
            ResponseEvent::class => 'finish',
        ];
    }

    public function start(): void
    {
        $this->startedAt = microtime(true);
    }

    public function finish(ResponseEvent $event): void
    {
        $request  = $event->getRequest();
        $response = $event->getResponse();

        $this->monitoringLogger->info('Page accessed', [
            'time'        => round(microtime(true) - $this->startedAt, 4),
            'memory'      => memory_get_usage(),
            'username'    => $this->tokenStorage->getToken()?->getUser()?->getUserIdentifier(),
            'route_name'  => $request->attributes->get('_route'),
            'method'      => $request->getMethod(),
            'uri'         => $request->getUri(),
            'status_code' => $response->getStatusCode(),
            'size'        => strlen($response->getContent()) * 8,
        ]);
    }
}
