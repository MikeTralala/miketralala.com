<?php

declare(strict_types=1);

namespace App\EventSubscriber;

use App\Messenger\Stamp\TrackerStamp;
use App\Tracker\TrackerIdGeneratorInterface;
use App\Tracker\TrackerVaultInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\RequestEvent;
use Symfony\Component\HttpKernel\Event\ResponseEvent;
use Symfony\Component\Messenger\Event\WorkerMessageReceivedEvent;

class TrackingSubscriber implements EventSubscriberInterface
{
    private const HEADER_NAME = 'X-tracker-id';

    public function __construct(
        private TrackerVaultInterface $vault,
        private TrackerIdGeneratorInterface $trackerIdGenerator
    ) {
    }

    public static function getSubscribedEvents(): array
    {
        return [
            RequestEvent::class               => ['initiateTracker', 10000],
            ResponseEvent::class              => 'exposeTrackerId',
            WorkerMessageReceivedEvent::class => 'setTracker',
        ];
    }

    public function initiateTracker(RequestEvent $event): void
    {
        $request = $event->getRequest();

        if (null !== $this->vault->getTrackingId()) {
            return;
        }

        if ($request->headers->has(self::HEADER_NAME)) {
            $this->vault->setTrackerId(
                $request->headers->get(self::HEADER_NAME)
            );

            return;
        }

        $this->vault->setTrackerId(
            $this->trackerIdGenerator->generate()
        );
    }

    public function setTracker(WorkerMessageReceivedEvent $event): void
    {
        $stamp = $event->getEnvelope()->last(TrackerStamp::class);
        if (null === $stamp) {
            return;
        }

        $this->vault->overrideTrackerId($stamp->getTrackerId());
    }

    public function exposeTrackerId(ResponseEvent $event): void
    {
        if ($this->vault->getTrackingId() === null) {
            return;
        }

        $event->getResponse()->headers->set(
            self::HEADER_NAME,
            $this->vault->getTrackingId()
        );
    }
}
