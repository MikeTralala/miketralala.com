<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;

interface Repliable
{
    public function getReplies(): Collection;

    public function addReply(BlogPostReply $reply): void;

    public function removeReply(BlogPostReply $reply): void;
}
