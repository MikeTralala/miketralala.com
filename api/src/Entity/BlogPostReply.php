<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use App\Resolver\AddBlogPostReplyToReplyResolver;
use App\Resolver\ChangeBlogPostReplyResolver;
use App\Resolver\LikeBlogPostReplyResolver;
use App\Resolver\RemoveBlogPostReplyFromReplyResolver;
use App\Resolver\RemoveBlogPostReplyResolver;
use App\Resolver\UnlikeBlogPostReplyResolver;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
#[ApiResource(
    graphql: [
        'item_query',
        'collection_query',
        'add'    => [
            'mutation'    => AddBlogPostReplyToReplyResolver::class,
            'deserialize' => false,
            'args'        => [
                'content' => ['type' => 'String!'],
                'postId'  => ['type' => 'ID'],
                'replyId' => ['type' => 'ID'],
            ],
        ],
        'change' => [
            'mutation'    => ChangeBlogPostReplyResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id'      => ['type' => 'ID!'],
                'title'   => ['type' => 'String!'],
                'content' => ['type' => 'String!'],
                'image'   => ['type' => 'String!'],
            ],
        ],
        'like'   => [
            'mutation'    => LikeBlogPostReplyResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
        'unlike' => [
            'mutation'    => UnlikeBlogPostReplyResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
        'remove' => [
            'mutation'    => RemoveBlogPostReplyResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
    ]
)]
#[ApiFilter(OrderFilter::class, properties: ['at'])]
class BlogPostReply implements Likeable, Repliable
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $author;

    #[ORM\Column(type: 'text', nullable: false)]
    private string $content;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeInterface $at;

    #[ORM\OneToMany(targetEntity: BlogPostLike::class, mappedBy: 'reply', cascade: ['persist', 'remove'])]
    private Collection $likes;

    #[ORM\OneToMany(targetEntity: BlogPostReply::class, mappedBy: 'parentReply', cascade: ['persist', 'remove'])]
    private Collection $replies;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    private DateTimeImmutable $lastUpdatedAt;

    #[ORM\ManyToOne(targetEntity: BlogPost::class, inversedBy: 'replies')]
    private ?BlogPost $post = null;

    #[ORM\ManyToOne(targetEntity: BlogPostReply::class, inversedBy: 'replies')]
    private ?BlogPostReply $parentReply = null;

    public function __construct(
        User $author,
        string $content
    ) {
        $this->replies = new ArrayCollection();
        $this->likes   = new ArrayCollection();

        $this->id            = Ulid::generate();
        $this->author        = $author;
        $this->content       = $content;
        $this->at            = new DateTimeImmutable();
        $this->lastUpdatedAt = new DateTimeImmutable();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAt(): DateTimeImmutable
    {
        return $this->at;
    }

    public function getLike(Criteria $criteria): ?BlogPostLike
    {
        return $this->getLikes($criteria)->first() ?: null;
    }

    public function getLikes(?Criteria $criteria = null): Collection
    {
        if ($criteria === null) {
            return $this->likes;
        }

        return $this->likes->matching($criteria);
    }

    public function addLike(BlogPostLike $like): void
    {
        $like->setReply($this);
        $this->likes->add($like);
    }

    public function removeLike(BlogPostLike $like): void
    {
        $this->likes->removeElement($like);
    }

    public function getReplies(?Criteria $criteria = null): Collection
    {
        if ($criteria === null) {
            return $this->replies;
        }

        return $this->replies->matching($criteria);
    }

    public function addReply(BlogPostReply $reply): void
    {
        $reply->setParentReply($this);
        $this->replies->add($reply);
    }

    public function removeReply(BlogPostReply $reply): void
    {
        $this->replies->removeElement($reply);
    }

    public function getLastUpdatedAt(): DateTimeImmutable
    {
        return $this->lastUpdatedAt;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        $this->content = $content;
        $this->updated();
    }

    public function getPost(): ?BlogPost
    {
        return $this->post;
    }

    public function setPost(BlogPost $post): void
    {
        $this->post = $post;
    }

    public function getParentReply(): ?BlogPostReply
    {
        return $this->parentReply;
    }

    public function setParentReply(BlogPostReply $parentReply): void
    {
        $this->parentReply = $parentReply;
    }

    public function getOriginalReply(): self
    {
        $parent = $this->getParentReply();
        if (null === $parent) {
            return $this;
        }

        return $parent->getOriginalReply();
    }

    public function getOriginalPost(): BlogPost
    {
        return $this->getOriginalReply()->getPost();
    }

    private function updated(): void
    {
        $this->lastUpdatedAt = new DateTimeImmutable();
    }
}
