<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use App\Entity\Traits\HasComparableId;
use App\Message\Command\DeactivateUser;
use App\Resolver\ActivateUserResolver;
use App\Resolver\DeactivateUserResolver;
use App\Resolver\RegisterUserResolver;
use DateTimeImmutable;
use Doctrine\ORM\Mapping as ORM;
use JetBrains\PhpStorm\Pure;
use Symfony\Component\Security\Core\User\PasswordAuthenticatedUserInterface;
use Symfony\Component\Security\Core\User\UserInterface;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
#[ORM\Table(name: '`user`')]
#[ApiResource(
    graphql: [
        'item_query',
        'collection_query',
        'register'   => [
            'mutation'    => RegisterUserResolver::class,
            'deserialize' => false,
            'args'        => [
                'email'    => ['type' => 'String!'],
                'name'     => ['type' => 'String!'],
                'password' => ['type' => 'String!'],
                'isAuthor' => ['type' => 'Boolean!'],
            ],
        ],
        'activate'   => [
            'mutation'    => ActivateUserResolver::class,
            'deserialize' => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
        'deactivate' => [
            'mutation'    => DeactivateUserResolver::class,
            'deserialize' => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
    ]
)]
class User implements UserInterface, PasswordAuthenticatedUserInterface
{
    use HasComparableId;

    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    #[ApiProperty(readable: true)]
    private string $id;

    #[ORM\Column(type: 'string', length: 255, unique: true)]
    private string $email;

    #[ORM\Column(type: 'string', length: 180, unique: false)]
    private string $name;

    #[ORM\Column(type: 'json')]
    private array $roles;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $activatedAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    private ?DateTimeImmutable $deactivatedAt;

    #[ORM\Column(type: 'string', nullable: false)]
    #[ApiProperty(readable: false, writable: false)]
    private string $password;

    public function __construct(
        string $email,
        string $name,
        array $roles = []
    ) {
        $this->id    = Ulid::generate();
        $this->email = $email;
        $this->name  = $name;
        $this->roles = $roles;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getName(): string
    {
        return $this->name;
    }

    public function getEmail(): string
    {
        return $this->email;
    }

    public function isActive(): bool
    {
        return $this->activatedAt !== null;
    }

    public function activate(): void
    {
        $this->deactivatedAt = null;
        $this->activatedAt   = new DateTimeImmutable();
    }

    public function deactivate(): void
    {
        $this->activatedAt   = null;
        $this->deactivatedAt = new DateTimeImmutable();
    }

    #[Pure] public function getUserIdentifier(): string
    {
        return $this->getEmail();
    }

    #[Pure] public function getRoles(): array
    {
        $roles = $this->roles;

        if ($this->isActive()) {
            $roles[] = Role::USER->name();
        }

        return array_unique($roles);
    }

    public function setRoles(array $roles): void
    {
        $this->roles = array_unique($roles);
    }

    public function eraseCredentials(): void
    {
    }

    public function getPassword(): string
    {
        return $this->password;
    }

    public function setPassword(string $password): void
    {
        $this->password = $password;
    }

    public function __toString(): string
    {
        dump(debug_backtrace());

        return $this->id;
    }
}
