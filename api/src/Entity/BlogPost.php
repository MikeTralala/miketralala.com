<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiFilter;
use ApiPlatform\Core\Annotation\ApiProperty;
use ApiPlatform\Core\Annotation\ApiResource;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\OrderFilter;
use ApiPlatform\Core\Bridge\Doctrine\Orm\Filter\SearchFilter;
use App\Resolver\AddBlogPostReplyToPostResolver;
use App\Resolver\AddBlogPostResolver;
use App\Resolver\ChangeBlogPostResolver;
use App\Resolver\LikeBlogPostResolver;
use App\Resolver\PublishBlogPostResolver;
use App\Resolver\RemoveBlogPostResolver;
use App\Resolver\UnlikeBlogPostResolver;
use DateTimeImmutable;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\Mapping as ORM;

#[ORM\Entity]
#[ApiResource(
    graphql: [
        'item_query',
        'collection_query' => [
            'filter' => ['publishedAt', 'status.id'],
        ],
        'add'              => [
            'mutation'    => AddBlogPostResolver::class,
            'deserialize' => false,
            'args'        => [
                'title'   => ['type' => 'String!'],
                'content' => ['type' => 'String!'],
                'image'   => ['type' => 'String!'],
            ],
        ],
        'change'           => [
            'mutation'    => ChangeBlogPostResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id'      => ['type' => 'ID!'],
                'title'   => ['type' => 'String!'],
                'content' => ['type' => 'String!'],
                'image'   => ['type' => 'String!'],
            ],
        ],
        'like'             => [
            'mutation'    => LikeBlogPostResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
        'unlike'           => [
            'mutation'    => UnlikeBlogPostResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
        'publish'          => [
            'mutation'    => PublishBlogPostResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
        'addReplyTo'       => [
            'mutation'    => AddBlogPostReplyToPostResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id'      => ['type' => 'ID!'],
                'content' => ['type' => 'String!'],
            ],
        ],
        'remove'           => [
            'mutation'    => RemoveBlogPostResolver::class,
            'deserialize' => false,
            'write'       => false,
            'args'        => [
                'id' => ['type' => 'ID!'],
            ],
        ],
    ]
)]
#[ApiFilter(OrderFilter::class, properties: ['publishedAt'])]
#[ApiFilter(SearchFilter::class, properties: ['status.id' => 'exact'])]
class BlogPost implements Likeable, Repliable
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\Column(type: 'string')]
    private string $title;

    #[ORM\Column(type: 'string')]
    private string $slug;

    #[ORM\Column(type: 'text')]
    private string $content;

    #[ORM\Column(type: 'string')]
    private string $image;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $author;

    #[ORM\ManyToOne(targetEntity: BlogPostStatus::class)]
    #[ORM\JoinColumn(nullable: false)]
    private BlogPostStatus $status;

    #[ORM\OneToMany(targetEntity: BlogPostState::class, mappedBy: 'post', cascade: ['persist', 'remove'])]
    #[ApiProperty(writable: false)]
    private Collection $states;

    #[ORM\OneToMany(targetEntity: BlogPostLike::class, mappedBy: 'post', cascade: ['persist', 'remove'])]
    #[ApiProperty(writable: false)]
    private Collection $likes;

    #[ORM\OneToMany(targetEntity: BlogPostReply::class, mappedBy: 'post', cascade: ['persist', 'remove'])]
    #[ApiProperty(writable: false)]
    private Collection $replies;

    #[ORM\Column(type: 'datetime_immutable', nullable: true)]
    #[ApiProperty(writable: false)]
    private ?DateTimeImmutable $publishedAt;

    #[ORM\Column(type: 'datetime_immutable', nullable: false)]
    #[ApiProperty(writable: false)]
    private DateTimeImmutable $lastUpdatedAt;

    public function __construct(
        string $id,
        string $title,
        string $slug,
        string $content,
        string $image,
        BlogPostState $state
    ) {
        $this->states  = new ArrayCollection();
        $this->replies = new ArrayCollection();
        $this->likes   = new ArrayCollection();

        $this->id            = $id;
        $this->title         = trim($title);
        $this->slug          = trim($slug);
        $this->content       = $content;
        $this->image         = $image;
        $this->author        = $state->getUser();
        $this->lastUpdatedAt = new DateTimeImmutable();
        $this->addState($state);
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAuthor(): User
    {
        return $this->author;
    }

    public function getStatus(): BlogPostStatus
    {
        return $this->status;
    }

    public function addState(BlogPostState $state): void
    {
        $state->setBlogPost($this);
        $this->status = $state->getStatus();
        $this->states->add($state);

        if ($state->getStatus()->is(BlogPostStatus::PUBLISHED)) {
            $this->publishedAt = $state->getAt();
        }
    }

    public function getStates(): Collection
    {
        return $this->states;
    }

    public function getLike(Criteria $criteria): ?BlogPostLike
    {
        return $this->getLikes($criteria)->first() ?: null;
    }

    public function getLikes(?Criteria $criteria = null): Collection
    {
        if ($criteria === null) {
            return $this->likes;
        }

        return $this->likes->matching($criteria);
    }

    public function addLike(BlogPostLike $like): void
    {
        $like->setPost($this);
        $this->likes->add($like);
    }

    public function removeLike(BlogPostLike $like): void
    {
        $this->likes->removeElement($like);
    }

    public function getReplies(?Criteria $criteria = null): Collection
    {
        if ($criteria === null) {
            return $this->replies;
        }

        return $this->replies->matching($criteria);
    }

    public function addReply(BlogPostReply $reply): void
    {
        $reply->setPost($this);
        $this->replies->add($reply);
    }

    public function removeReply(BlogPostReply $reply): void
    {
        $this->replies->removeElement($reply);
    }

    public function getTitle(): string
    {
        return $this->title;
    }

    public function setTitle(string $title): void
    {
        if ($this->title === $title) {
            return;
        }

        $this->updated();
        $this->title = $title;
    }

    public function getSlug(): string
    {
        return $this->slug;
    }

    public function getContent(): string
    {
        return $this->content;
    }

    public function setContent(string $content): void
    {
        if ($this->content === $content) {
            return;
        }

        $this->updated();
        $this->content = $content;
    }

    public function getImage(): string
    {
        return $this->image;
    }

    public function setImage(string $image): void
    {
        if ($this->image === $image) {
            return;
        }

        $this->updated();
        $this->image = $image;
    }

    public function getPublishedAt(): ?DateTimeImmutable
    {
        return $this->publishedAt;
    }

    public function getLastUpdatedAt(): DateTimeImmutable
    {
        return $this->lastUpdatedAt;
    }

    private function updated(): void
    {
        $this->lastUpdatedAt = new DateTimeImmutable();
    }
}
