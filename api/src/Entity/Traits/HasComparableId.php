<?php

declare(strict_types=1);

namespace App\Entity\Traits;

use LogicException;

trait HasComparableId
{

    /**
     * Checks whether this object is the same as the passed value.
     *
     * Returns true when:
     * - passed value is strictly the same object.
     * - passed value is of the same class and the result of ::getId() is strictly the same.
     * - passed value is an array and an ::is() call on at least one of the values returns true.
     * - passed value is strictly the same to ::getId() of this object.
     *
     * Note: This method expects ::getId() to be defined on the implementing class.
     *
     * @param int|string|array|object $id
     *
     * @return bool
     */
    public function is(int|string|array|object $id): bool
    {
        if ($this === $id) { // strict comparison (same object)
            return true;
        }

        if (! method_exists($this, 'getId')) {
            throw new LogicException(sprintf('getId must be defined in %s', $this::class));
        }

        if ($this->getId() === null) { // consider null to never be the same
            return false;
        }

        if (is_object($id)) {
            if ($id instanceof self) {
                return $this->getId() === $id->getId();
            }

            // if the passed value is not of the same class, it is never going to be the same
            return false;
        }

        // compare 'or' against multiple
        if (is_array($id)) {
            foreach ($id as $childId) {
                if ($this->is($childId)) {
                    return true;
                }
            }

            return false;
        }

        // comparison for scalar value against this id (fails if anything else or scalar value is not strictly equals)
        return $this->getId() === $id;
    }
}
