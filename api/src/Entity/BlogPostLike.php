<?php

declare(strict_types=1);

namespace App\Entity;

use ApiPlatform\Core\Annotation\ApiResource;
use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
#[ApiResource]
class BlogPostLike
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: User::class)]
    #[ORM\JoinColumn(nullable: false)]
    private User $by;
    
    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeInterface $at;

    #[ORM\ManyToOne(targetEntity: BlogPost::class, inversedBy: 'likes')]
    private ?BlogPost $post;

    #[ORM\ManyToOne(targetEntity: BlogPostReply::class, inversedBy: 'likes')]
    private ?BlogPostReply $reply;

    public function __construct(User $by)
    {
        $this->id = Ulid::generate();
        $this->by = $by;
        $this->at = new DateTimeImmutable();
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAt(): DateTimeInterface
    {
        return $this->at;
    }

    public function getBy(): User
    {
        return $this->by;
    }

    public function getPost(): ?BlogPost
    {
        return $this->post;
    }

    public function setPost(BlogPost $post): void
    {
        $this->post = $post;
    }

    public function getParentReply(): ?BlogPostReply
    {
        return $this->reply;
    }

    public function setReply(BlogPostReply $reply): void
    {
        $this->reply = $reply;
    }

}
