<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;

interface Likeable
{
    public function getLike(Criteria $criteria): ?BlogPostLike;

    public function getLikes(?Criteria $criteria = null): Collection;

    public function addLike(BlogPostLike $like): void;

    public function removeLike(BlogPostLike $like): void;
}
