<?php

declare(strict_types=1);

namespace App\Entity;

use DateTimeImmutable;
use DateTimeInterface;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Uid\Ulid;

#[ORM\Entity]
class BlogPostState
{
    #[ORM\Id]
    #[ORM\Column(type: 'ulid')]
    private string $id;

    #[ORM\ManyToOne(targetEntity: BlogPostStatus::class)]
    private BlogPostStatus $status;

    #[ORM\ManyToOne(targetEntity: User::class)]
    private User $user;

    #[ORM\Column(type: 'datetime_immutable')]
    private DateTimeInterface $at;

    #[ORM\ManyToOne(targetEntity: BlogPost::class, inversedBy: 'states')]
    private ?BlogPost $post;

    public function __construct(
        BlogPostStatus $status,
        User $user
    ) {
        $this->id     = Ulid::generate();
        $this->status = $status;
        $this->user   = $user;
        $this->at     = new DateTimeImmutable();
    }

    public function setBlogPost(BlogPost $post): void
    {
        $this->post = $post;
    }

    public function getId(): string
    {
        return $this->id;
    }

    public function getAt(): DateTimeImmutable|DateTimeInterface
    {
        return $this->at;
    }

    public function getPost(): BlogPost
    {
        return $this->post;
    }

    public function getStatus(): BlogPostStatus
    {
        return $this->status;
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
