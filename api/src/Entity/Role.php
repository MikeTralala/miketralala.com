<?php

declare(strict_types=1);

namespace App\Entity;

enum Role: string
{
    case USER = 'ROLE_USER';
    case AUTHOR = 'ROLE_AUTHOR';
    case ADMINISTRATOR = 'ROLE_ADMINISTRATOR';

    public function name(): string
    {
        return $this->value;
    }
}
