<?php

declare(strict_types=1);

namespace App\Logger;

use App\Tracker\TrackerVaultInterface;

class TrackerIdProcessor
{
    public function __construct(private TrackerVaultInterface $vault) { }

    public function __invoke(array $record): array
    {
        $record['tracker_id'] = $this->vault->getTrackingId();

        return $record;
    }
}
