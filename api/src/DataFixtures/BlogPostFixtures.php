<?php

declare(strict_types=1);

namespace App\DataFixtures;

use App\Entity\BlogPost;
use App\Entity\BlogPostLike;
use App\Entity\BlogPostReply;
use App\Entity\BlogPostState;
use App\Entity\BlogPostStatus;
use App\Helper\Utilities;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Common\DataFixtures\DependentFixtureInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\Uid\Ulid;

final class BlogPostFixtures extends Fixture implements DependentFixtureInterface
{
    public function load(ObjectManager $manager): void
    {
        $manager->persist($this->createBlogPostLoremIpsum());
        sleep(1);
        $manager->persist($this->createBlogPostMaurisSedNisi());
        sleep(1);

        $manager->persist($this->createBlogPostDolarSit());
        $manager->flush();
    }

    private function createBlogPostLoremIpsum(): BlogPost
    {
        $post = $this->createPost(
            'Lorem ipsum',
            'programming/compiled-code-blurred',
            'mike@miketralala.com',
            'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id lacinia sapien, euismod accumsan turpis. Duis vitae finibus dolor. Proin id porttitor neque. Pellentesque dapibus aliquam sapien ac gravida. Cras nec augue venenatis urna sollicitudin vehicula. Integer non ornare ligula. Fusce dictum pharetra quam in aliquam.

Nulla feugiat est et sem ullamcorper, nec venenatis odio sagittis. Aliquam eget tincidunt metus. Suspendisse at ligula non ipsum iaculis vulputate nec quis enim. Cras et felis vitae elit aliquam pulvinar. Ut id nisl ultricies, aliquet erat a, tincidunt turpis. Ut suscipit dictum tellus, sit amet bibendum velit rhoncus id. Pellentesque sagittis augue quis nibh ultrices ultricies. Nullam scelerisque, lorem ac auctor iaculis, lectus enim congue tellus, eget dapibus dolor purus nec purus. Suspendisse sit amet metus vitae mi consequat vestibulum. Ut vulputate pulvinar mauris id commodo. Proin pretium malesuada orci, egestas pretium nulla scelerisque nec. In volutpat convallis ante ullamcorper imperdiet. Nam efficitur velit sed dolor feugiat, nec commodo ex commodo. Sed sed mi finibus, tincidunt massa quis, efficitur sem. Sed ut lacinia est.'
        );
        $post->addState(new BlogPostState($this->getReference(BlogPostStatus::PUBLISHED), $this->getReference('john@doe.com')));

        $post->addLike($this->createLike('john@doe.com'));
        $post->addLike($this->createLike('jane@doe.com'));

        $reply = $this->createReply(
            'john@doe.com',
            'Vestibulum pulvinar metus quis pellentesque molestie. Quisque neque leo, lobortis eu ullamcorper sagittis, rhoncus ac mauris. Maecenas porta velit congue feugiat mollis. Nulla malesuada tincidunt tincidunt. Proin pulvinar pellentesque convallis. Praesent dictum, arcu eu gravida mattis, velit nulla tincidunt massa, sit amet sodales odio erat et dui. Quisque tempus nunc ut neque mattis, vel vehicula elit dignissim. Etiam cursus venenatis ex a vehicula. Nam tincidunt egestas ante. Fusce eget ante tincidunt, placerat augue sit amet, facilisis urna. Ut vitae ornare felis, sit amet pulvinar lacus.'
        );
        $reply->addLike($this->createLike('mike@miketralala.com'));
        $reply->addLike($this->createLike('jane@doe.com'));

        $childReply = $this->createReply(
            'jane@doe.com',
            'Suspendisse at ligula non ipsum iaculis vulputate nec quis enim. Cras et felis vitae elit aliquam pulvinar. Ut id nisl ultricies, aliquet erat a, tincidunt turpis. Ut suscipit dictum tellus, sit amet bibendum velit rhoncus id.'
        );
        $childReply->addLike($this->createLike('john@doe.com'));
        $reply->addReply($childReply);

        $childChildReply = $this->createReply(
            'jane@doe.com',
            'Morbi sit amet magna eu sem mollis efficitur. Nunc non accumsan mauris, nec faucibus turpis. Aliquam porta eleifend nibh. Phasellus eget dolor sed elit mollis ornare at sed nisi. Sed a enim efficitur, rhoncus enim quis, vestibulum felis. Cras vehicula non justo sollicitudin lacinia. '
        );
        $childChildReply->addLike($this->createLike('john@doe.com'));
        $childReply->addReply($childReply);

        $anotherChildReply = $this->createReply(
            'mike@miketralala.com',
            'Quisque mauris tortor, mollis in neque non, posuere pulvinar odio. Nam nec odio quis nisl pretium sollicitudin sed et felis. '
        );
        $anotherChildReply->addLike($this->createLike('john@doe.com'));
        $anotherChildReply->addLike($this->createLike('jane@doe.com'));
        $reply->addReply($anotherChildReply);

        return $post;
    }

    private function createBlogPostMaurisSedNisi(): BlogPost
    {
        $post = $this->createPost(
            'Mauris sed nisi',
            'general/colors-liquid',
            'john@doe.com',
            'Mauris sed nisi in risus euismod varius nec id augue. Morbi suscipit sagittis augue, non fringilla justo aliquam a. In feugiat tincidunt urna, id tincidunt sem tristique posuere. Quisque mi magna, vehicula vel nisi sit amet, feugiat sollicitudin ipsum. In enim ipsum, mattis ut enim quis, feugiat rhoncus metus. Nam non dolor ac eros dignissim lacinia. Suspendisse et eros nec ante cursus vestibulum at in est. Morbi pellentesque magna purus, et efficitur odio sollicitudin vulputate. Maecenas non arcu tellus. Nunc sed nisi pretium, fermentum justo sit amet, dapibus nunc. Integer auctor bibendum nisi, non placerat mauris mattis ut. Donec auctor, est suscipit pulvinar eleifend, magna elit aliquet odio, in euismod mi ex nec elit. Integer sem nunc, porttitor sed euismod id, vehicula ut ante.

Suspendisse ullamcorper enim vel mi viverra, at dignissim sem mollis. Nullam ac ornare diam. Fusce egestas, mauris et euismod facilisis, mi nibh dapibus magna, in malesuada mi sapien vitae dolor. Suspendisse dolor nulla, tincidunt sit amet convallis non, vehicula bibendum mauris. Integer et lacinia urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempor lacus nec facilisis ullamcorper. Suspendisse porta lectus mauris, sed tincidunt arcu luctus a. Nullam mauris turpis, commodo at ligula ornare, blandit dictum felis. Suspendisse nec aliquet nibh. Fusce lobortis sollicitudin ex vitae suscipit. Sed rhoncus facilisis erat molestie ultrices. Donec sodales ullamcorper urna, sollicitudin condimentum ligula consectetur ac. Proin quis accumsan dui, ut placerat diam. Cras aliquam odio nec quam bibendum, eu dignissim nisl aliquet.

In cursus sem vitae eros gravida, at gravida magna euismod. Curabitur ut sollicitudin erat, at viverra justo. Nam id cursus lorem, quis interdum ante. In hac habitasse platea dictumst. Suspendisse lectus risus, ullamcorper ac venenatis eu, interdum vitae tellus. Phasellus ac mollis sapien, sed porta justo. Etiam justo ante, suscipit sit amet gravida vitae, hendrerit id odio.'
        );

        $post->addState(new BlogPostState($this->getReference(BlogPostStatus::PUBLISHED), $this->getReference('john@doe.com')));
        $post->addLike($this->createLike('jane@doe.com'));

        return $post;
    }

    private function createBlogPostDolarSit(): BlogPost
    {
        return $this->createPost(
            'Dolar sit',
            'data/matrix',
            'john@doe.com',
            'Morbi sit amet magna eu sem mollis efficitur. Nunc non accumsan mauris, nec faucibus turpis. Aliquam porta eleifend nibh. Phasellus eget dolor sed elit mollis ornare at sed nisi. Sed a enim efficitur, rhoncus enim quis, vestibulum felis. Cras vehicula non justo sollicitudin lacinia. Vestibulum a volutpat lectus.

Fusce arcu erat, fermentum quis dolor sed, efficitur aliquam risus. Vestibulum tristique hendrerit velit, at gravida purus dictum auctor. Nam porta ut nulla eu vehicula. Quisque mauris tortor, mollis in neque non, posuere pulvinar odio. Nam nec odio quis nisl pretium sollicitudin sed et felis. Pellentesque malesuada consectetur posuere. Nullam accumsan urna in eros finibus, vel ultricies nunc facilisis. Quisque id libero ac dolor euismod tempor. Donec at libero congue, venenatis libero non, bibendum lacus.'
        );
    }

    private function createPost(string $title, string $image, string $user, string $content): BlogPost
    {
        $post = new BlogPost(
            Ulid::generate(),
            $title,
            Utilities::slugify($title),
            $content,
            $image,
            new BlogPostState(
                $this->getReference(BlogPostStatus::DRAFT),
                $this->getReference($user)
            )
        );


        return $post;
    }

    private function createReply(string $user, string $content): BlogPostReply
    {
        return new BlogPostReply(
            $this->getReference($user),
            $content
        );
    }

    private function createLike(string $user)
    {
        return new BlogPostLike(
            $this->getReference($user)
        );
    }

    public static function getGroups(): array
    {
        return ['development'];
    }

    public function getDependencies(): array
    {
        return [
            MasterDataFixtures::class,
            UserFixtures::class,
        ];
    }
}
