<?php

namespace App\DataFixtures;

use App\Entity\Role;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Bundle\FixturesBundle\FixtureGroupInterface;
use Doctrine\Persistence\ObjectManager;
use Symfony\Component\PasswordHasher\Hasher\UserPasswordHasherInterface;

final class UserFixtures extends Fixture implements FixtureGroupInterface
{
    public function __construct(
        private UserPasswordHasherInterface $passwordHasher
    ) {
    }

    public function load(ObjectManager $manager): void
    {
        $users = [
            ['mike@miketralala.com', 'Michael de Bie', 'w63Cv^k&QkZNB2&FE&iG%sf2', [Role::AUTHOR->name(), Role::ADMINISTRATOR->name()]],
            ['jane@doe.com', 'Jane Doe', 'fj39n4c089wb7R348379WF3_Fe&iG', [Role::AUTHOR->name()]],
            ['john@doe.com', 'John Doe', '23n4c089wb7R.348379WffE&iG', [Role::AUTHOR->name()]],
        ];

        foreach ($users as $data) {
            $user = $this->createUser(...$data);
            $manager->persist($user);

            $this->addReference($user->getEmail(), $user);
        }

        $manager->flush();
    }

    private function createUser(string $email, string $name, string $password, array $roles): User
    {
        $user = new User($email, $name, $roles);

        $user->setPassword(
            $this->passwordHasher->hashPassword($user, $password)
        );
        $user->activate();

        return $user;
    }

    public static function getGroups(): array
    {
        return ['development'];
    }
}
