<?php

namespace App\DataFixtures;

use App\Entity\BlogPostStatus;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

final class MasterDataFixtures extends Fixture
{

    public function load(ObjectManager $manager): void
    {
        $statuses = [
            [BlogPostStatus::DRAFT, 'Draft'],
            [BlogPostStatus::PUBLISHED, 'Published'],
            [BlogPostStatus::REMOVED, 'Removed'],
        ];

        foreach ($statuses as $data) {
            $status = new BlogPostStatus(...$data);
            $manager->persist($status);
            $this->addReference($status->getId(), $status);
        }

        $manager->flush();
    }

    public static function getGroups(): array
    {
        return ['master-data'];
    }
}
