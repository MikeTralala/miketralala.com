<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220116150953 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE TABLE blog_post (
          id UUID NOT NULL,
          author_id UUID NOT NULL,
          status_id INT NOT NULL,
          title VARCHAR(255) NOT NULL,
          slug VARCHAR(255) NOT NULL,
          content TEXT NOT NULL,
          image VARCHAR(255) NOT NULL,
          published_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
          last_updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_BA5AE01DF675F31B ON blog_post (author_id)');
        $this->addSql('CREATE INDEX IDX_BA5AE01D6BF700BD ON blog_post (status_id)');
        $this->addSql('COMMENT ON COLUMN blog_post.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post.author_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post.published_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN blog_post.last_updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE blog_post_like (
          id UUID NOT NULL,
          by_id UUID NOT NULL,
          post_id UUID DEFAULT NULL,
          reply_id UUID DEFAULT NULL,
          at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_C048CFF7AAE72004 ON blog_post_like (by_id)');
        $this->addSql('CREATE INDEX IDX_C048CFF74B89032C ON blog_post_like (post_id)');
        $this->addSql('CREATE INDEX IDX_C048CFF78A0E4E7F ON blog_post_like (reply_id)');
        $this->addSql('COMMENT ON COLUMN blog_post_like.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_like.by_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_like.post_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_like.reply_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_like.at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE blog_post_reply (
          id UUID NOT NULL,
          author_id UUID NOT NULL,
          post_id UUID DEFAULT NULL,
          parent_reply_id UUID DEFAULT NULL,
          content TEXT NOT NULL,
          at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
          last_updated_at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_8C7568E6F675F31B ON blog_post_reply (author_id)');
        $this->addSql('CREATE INDEX IDX_8C7568E64B89032C ON blog_post_reply (post_id)');
        $this->addSql('CREATE INDEX IDX_8C7568E615C3E2AE ON blog_post_reply (parent_reply_id)');
        $this->addSql('COMMENT ON COLUMN blog_post_reply.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_reply.author_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_reply.post_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_reply.parent_reply_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_reply.at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_reply.last_updated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE blog_post_state (
          id UUID NOT NULL,
          status_id INT DEFAULT NULL,
          user_id UUID DEFAULT NULL,
          post_id UUID DEFAULT NULL,
          at TIMESTAMP(0) WITHOUT TIME ZONE NOT NULL,
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE INDEX IDX_D24E7CFD6BF700BD ON blog_post_state (status_id)');
        $this->addSql('CREATE INDEX IDX_D24E7CFDA76ED395 ON blog_post_state (user_id)');
        $this->addSql('CREATE INDEX IDX_D24E7CFD4B89032C ON blog_post_state (post_id)');
        $this->addSql('COMMENT ON COLUMN blog_post_state.id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_state.user_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_state.post_id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN blog_post_state.at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('CREATE TABLE blog_post_status (
          id INT NOT NULL,
          name VARCHAR(255) NOT NULL,
          code VARCHAR(255) NOT NULL,
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE TABLE "user" (
          id UUID NOT NULL,
          email VARCHAR(255) NOT NULL,
          name VARCHAR(180) NOT NULL,
          roles JSON NOT NULL,
          activated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
          deactivated_at TIMESTAMP(0) WITHOUT TIME ZONE DEFAULT NULL,
          password VARCHAR(255) NOT NULL,
          PRIMARY KEY(id)
        )');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_8D93D649E7927C74 ON "user" (email)');
        $this->addSql('COMMENT ON COLUMN "user".id IS \'(DC2Type:ulid)\'');
        $this->addSql('COMMENT ON COLUMN "user".activated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('COMMENT ON COLUMN "user".deactivated_at IS \'(DC2Type:datetime_immutable)\'');
        $this->addSql('ALTER TABLE
          blog_post
        ADD
          CONSTRAINT FK_BA5AE01DF675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post
        ADD
          CONSTRAINT FK_BA5AE01D6BF700BD FOREIGN KEY (status_id) REFERENCES blog_post_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_like
        ADD
          CONSTRAINT FK_C048CFF7AAE72004 FOREIGN KEY (by_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_like
        ADD
          CONSTRAINT FK_C048CFF74B89032C FOREIGN KEY (post_id) REFERENCES blog_post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_like
        ADD
          CONSTRAINT FK_C048CFF78A0E4E7F FOREIGN KEY (reply_id) REFERENCES blog_post_reply (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_reply
        ADD
          CONSTRAINT FK_8C7568E6F675F31B FOREIGN KEY (author_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_reply
        ADD
          CONSTRAINT FK_8C7568E64B89032C FOREIGN KEY (post_id) REFERENCES blog_post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_reply
        ADD
          CONSTRAINT FK_8C7568E615C3E2AE FOREIGN KEY (parent_reply_id) REFERENCES blog_post_reply (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_state
        ADD
          CONSTRAINT FK_D24E7CFD6BF700BD FOREIGN KEY (status_id) REFERENCES blog_post_status (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_state
        ADD
          CONSTRAINT FK_D24E7CFDA76ED395 FOREIGN KEY (user_id) REFERENCES "user" (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
        $this->addSql('ALTER TABLE
          blog_post_state
        ADD
          CONSTRAINT FK_D24E7CFD4B89032C FOREIGN KEY (post_id) REFERENCES blog_post (id) NOT DEFERRABLE INITIALLY IMMEDIATE');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE SCHEMA public');
        $this->addSql('ALTER TABLE blog_post_like DROP CONSTRAINT FK_C048CFF74B89032C');
        $this->addSql('ALTER TABLE blog_post_reply DROP CONSTRAINT FK_8C7568E64B89032C');
        $this->addSql('ALTER TABLE blog_post_state DROP CONSTRAINT FK_D24E7CFD4B89032C');
        $this->addSql('ALTER TABLE blog_post_like DROP CONSTRAINT FK_C048CFF78A0E4E7F');
        $this->addSql('ALTER TABLE blog_post_reply DROP CONSTRAINT FK_8C7568E615C3E2AE');
        $this->addSql('ALTER TABLE blog_post DROP CONSTRAINT FK_BA5AE01D6BF700BD');
        $this->addSql('ALTER TABLE blog_post_state DROP CONSTRAINT FK_D24E7CFD6BF700BD');
        $this->addSql('ALTER TABLE blog_post DROP CONSTRAINT FK_BA5AE01DF675F31B');
        $this->addSql('ALTER TABLE blog_post_like DROP CONSTRAINT FK_C048CFF7AAE72004');
        $this->addSql('ALTER TABLE blog_post_reply DROP CONSTRAINT FK_8C7568E6F675F31B');
        $this->addSql('ALTER TABLE blog_post_state DROP CONSTRAINT FK_D24E7CFDA76ED395');
        $this->addSql('DROP TABLE blog_post');
        $this->addSql('DROP TABLE blog_post_like');
        $this->addSql('DROP TABLE blog_post_reply');
        $this->addSql('DROP TABLE blog_post_state');
        $this->addSql('DROP TABLE blog_post_status');
        $this->addSql('DROP TABLE "user"');
    }
}
