import moment from 'moment'
import _ from 'lodash'
import Vue from 'vue'

export const state = () => ({
  list: {
    '7081dcda-6465-4f4c-b2ed-4569cad7e92c': {
      id: '7081dcda-6465-4f4c-b2ed-4569cad7e92c',
      state: 'PUBLISHED',
      title: 'Same shit, different day',
      lastEditedAt: moment().subtract(_.random(2, 12), 'hours').subtract(_.random(0, 60), 'minutes'),
      author: {
        id: '017e2721-d049-690f-4088-2e7a94f65884',
        name: 'Michael de Bie',
        email: 'mike@miketrala.com',
        roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
      },
      image: 'one-liners/same-day-different-shit',
      content: `Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas id lacinia sapien, euismod accumsan turpis. Duis vitae finibus dolor. Proin id porttitor neque. Pellentesque dapibus aliquam sapien ac gravida. Cras nec augue venenatis urna sollicitudin vehicula. Integer non ornare ligula. Fusce dictum pharetra quam in aliquam.\n

Nulla feugiat est et sem ullamcorper, nec venenatis odio sagittis. Aliquam eget tincidunt metus. Suspendisse at ligula non ipsum iaculis vulputate nec quis enim. Cras et felis vitae elit aliquam pulvinar. Ut id nisl ultricies, aliquet erat a, tincidunt turpis. Ut suscipit dictum tellus, sit amet bibendum velit rhoncus id. Pellentesque sagittis augue quis nibh ultrices ultricies. Nullam scelerisque, lorem ac auctor iaculis, lectus enim congue tellus, eget dapibus dolor purus nec purus. Suspendisse sit amet metus vitae mi consequat vestibulum. Ut vulputate pulvinar mauris id commodo. Proin pretium malesuada orci, egestas pretium nulla scelerisque nec. In volutpat convallis ante ullamcorper imperdiet. Nam efficitur velit sed dolor feugiat, nec commodo ex commodo. Sed sed mi finibus, tincidunt massa quis, efficitur sem. Sed ut lacinia est.`,
      replies: {
        '7081dcda-6465-4f4c-b2ed-4569cad7e92b': {
          id: '7081dcda-6465-4f4c-b2ed-4569cad7e92b',
          author: {
            id: '017e2721-d049-690f-4088-2e7a94f65821',
            name: 'John Doe',
            email: 'John@doe.com',
            roles: ['ROLE_USER']
          },
          at: moment().subtract(_.random(30, 60), 'minutes'),
          lastEditedAt: null,
          content: 'Vestibulum pulvinar metus quis pellentesque molestie. Quisque neque leo, lobortis eu ullamcorper sagittis, rhoncus ac mauris. Maecenas porta velit congue feugiat mollis. Nulla malesuada tincidunt tincidunt. Proin pulvinar pellentesque convallis. Praesent dictum, arcu eu gravida mattis, velit nulla tincidunt massa, sit amet sodales odio erat et dui. Quisque tempus nunc ut neque mattis, vel vehicula elit dignissim. Etiam cursus venenatis ex a vehicula. Nam tincidunt egestas ante. Fusce eget ante tincidunt, placerat augue sit amet, facilisis urna. Ut vitae ornare felis, sit amet pulvinar lacus.',
          replies: {
            '7081dcda-6465-4f4c-b2ed-4569cad7e92b': {
              id: '7081dcda-6465-4f4c-b2ed-4569cad7e92b',
              author: {
                id: '017e2721-d049-690f-3088-2e7a94f65821',
                name: 'Angela Sturm',
                email: 'angela@de_toet.nl',
                roles: ['ROLE_USER', 'ROLE_AUTHOR']
              },
              at: moment().subtract(_.random(0, 30), 'minutes'),
              lastEditedAt: null,
              content: 'Suspendisse at ligula non ipsum iaculis vulputate nec quis enim. Cras et felis vitae elit aliquam pulvinar. Ut id nisl ultricies, aliquet erat a, tincidunt turpis. Ut suscipit dictum tellus, sit amet bibendum velit rhoncus id.',
              replies: {
                '7081dcda-6465-4f4c-b2ed-456ghad7e92b': {
                  id: '7081dcda-6465-4f4c-b2ed-456ghad7e92b',
                  author: {
                    id: '017e2721-d049-690f-4088-2e7a94f65884',
                    name: 'Michael de Bie',
                    email: 'mike@miketrala.com',
                    roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
                  },
                  at: moment().subtract(_.random(0, 30), 'minutes'),
                  lastEditedAt: null,
                  content: 'Suspendisse at ligula non ipsum iaculis vulputate nec quis enim. Cras et felis vitae elit aliquam pulvinar. Ut id nisl ultricies, aliquet erat a, tincidunt turpis. Ut suscipit dictum tellus, sit amet bibendum velit rhoncus id.',
                  replies: {},
                  likes: {}
                }
              },
              likes: {}
            }
          },
          likes: {}
        }
      },
      likes: {
        '017e2721-d049-690f-3088-2esdf4f65821': {
          id: '017e2721-d049-690f-3088-2esdf4f65821',
          at: moment(),
          by: {
            id: '017e2721-d049-690f-3088-2e7a94f65821',
            name: 'Angela Sturm',
            email: 'angela@de_toet.nl',
            roles: ['ROLE_USER', 'ROLE_AUTHOR']
          }
        },
        '017e2721-d049-690f-0834-2e7a94f65821': {
          id: '017e2721-d049-690f-0834-2e7a94f65821',
          at: moment(),
          by: {
            id: '017e2721-d049-690f-0834-2e7a94f65821',
            name: 'John Doe',
            email: 'John@doe.com',
            roles: ['ROLE_USER']
          }
        }
      }
    },
    '744855d8-f099-4d56-8d41-a005702b1b0f': {
      id: '744855d8-f099-4d56-8d41-a005702b1b0f',
      state: 'PUBLISHED',
      title: 'Dolar sit',
      lastEditedAt: moment().subtract(_.random(2, 4), 'days').subtract(_.random(0, 24), 'hours').subtract(_.random(0, 60), 'minutes'),
      image: 'data/matrix',
      author: {
        id: '017e2721-d049-690f-3088-2e7a94f65821',
        name: 'Angela Sturm',
        email: 'angela@de_toet.nl',
        roles: ['ROLE_USER', 'ROLE_AUTHOR']
      },
      content: `Morbi sit amet magna eu sem mollis efficitur. Nunc non accumsan mauris, nec faucibus turpis. Aliquam porta eleifend nibh. Phasellus eget dolor sed elit mollis ornare at sed nisi. Sed a enim efficitur, rhoncus enim quis, vestibulum felis. Cras vehicula non justo sollicitudin lacinia. Vestibulum a volutpat lectus.

Fusce arcu erat, fermentum quis dolor sed, efficitur aliquam risus. Vestibulum tristique hendrerit velit, at gravida purus dictum auctor. Nam porta ut nulla eu vehicula. Quisque mauris tortor, mollis in neque non, posuere pulvinar odio. Nam nec odio quis nisl pretium sollicitudin sed et felis. Pellentesque malesuada consectetur posuere. Nullam accumsan urna in eros finibus, vel ultricies nunc facilisis. Quisque id libero ac dolor euismod tempor. Donec at libero congue, venenatis libero non, bibendum lacus.`,
      replies: {},
      likes: {}
    },
    '8eaa5bd5-e08f-483b-be81-cd726dac3ed4': {
      id: '8eaa5bd5-e08f-483b-be81-cd726dac3ed4',
      state: 'PUBLISHED',
      title: 'Mauris sed nisi',
      lastEditedAt: moment().subtract(_.random(4, 10), 'days').subtract(_.random(0, 24), 'hours').subtract(_.random(0, 60), 'minutes'),
      image: 'architecture/flow',
      author: {
        id: '017e2721-d049-690f-4088-2e7a94f65884',
        name: 'Michael de Bie',
        email: 'mike@miketrala.com',
        roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
      },
      content: `Mauris sed nisi in risus euismod varius nec id augue. Morbi suscipit sagittis augue, non fringilla justo aliquam a. In feugiat tincidunt urna, id tincidunt sem tristique posuere. Quisque mi magna, vehicula vel nisi sit amet, feugiat sollicitudin ipsum. In enim ipsum, mattis ut enim quis, feugiat rhoncus metus. Nam non dolor ac eros dignissim lacinia. Suspendisse et eros nec ante cursus vestibulum at in est. Morbi pellentesque magna purus, et efficitur odio sollicitudin vulputate. Maecenas non arcu tellus. Nunc sed nisi pretium, fermentum justo sit amet, dapibus nunc. Integer auctor bibendum nisi, non placerat mauris mattis ut. Donec auctor, est suscipit pulvinar eleifend, magna elit aliquet odio, in euismod mi ex nec elit. Integer sem nunc, porttitor sed euismod id, vehicula ut ante.

Suspendisse ullamcorper enim vel mi viverra, at dignissim sem mollis. Nullam ac ornare diam. Fusce egestas, mauris et euismod facilisis, mi nibh dapibus magna, in malesuada mi sapien vitae dolor. Suspendisse dolor nulla, tincidunt sit amet convallis non, vehicula bibendum mauris. Integer et lacinia urna. Lorem ipsum dolor sit amet, consectetur adipiscing elit. Donec tempor lacus nec facilisis ullamcorper. Suspendisse porta lectus mauris, sed tincidunt arcu luctus a. Nullam mauris turpis, commodo at ligula ornare, blandit dictum felis. Suspendisse nec aliquet nibh. Fusce lobortis sollicitudin ex vitae suscipit. Sed rhoncus facilisis erat molestie ultrices. Donec sodales ullamcorper urna, sollicitudin condimentum ligula consectetur ac. Proin quis accumsan dui, ut placerat diam. Cras aliquam odio nec quam bibendum, eu dignissim nisl aliquet.

In cursus sem vitae eros gravida, at gravida magna euismod. Curabitur ut sollicitudin erat, at viverra justo. Nam id cursus lorem, quis interdum ante. In hac habitasse platea dictumst. Suspendisse lectus risus, ullamcorper ac venenatis eu, interdum vitae tellus. Phasellus ac mollis sapien, sed porta justo. Etiam justo ante, suscipit sit amet gravida vitae, hendrerit id odio.`,
      replies: {},
      likes: {}
    },
    '5e15fdab-6088-40e2-9db8-37031b2289ea': {
      id: '5e15fdab-6088-40e2-9db8-37031b2289ea',
      state: 'PUBLISHED',
      title: 'Ipsum semper efficitur',
      lastEditedAt: moment().subtract(_.random(10, 11), 'days').subtract(_.random(0, 24), 'hours').subtract(_.random(0, 60), 'minutes'),
      image: 'security/key',
      author: {
        id: '017e2721-d049-690f-4088-2e7a94f65884',
        name: 'Michael de Bie',
        email: 'mike@miketrala.com',
        roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
      },
      content: `Phasellus vulputate ex blandit ipsum semper efficitur. Nam maximus nisi et nisl posuere lobortis. Sed vestibulum rutrum elit cursus lobortis. Etiam consequat, massa ultricies ultrices lacinia, est mi placerat ante, in tristique elit eros in eros. Vestibulum a ante faucibus, cursus arcu eu, tristique urna. Etiam pulvinar lorem ac lectus maximus sagittis. Integer convallis enim non urna tristique, at tristique nisl lacinia. Ut scelerisque tincidunt ligula id bibendum. Quisque efficitur sapien quis bibendum luctus. Maecenas non urna at est imperdiet ornare. Ut semper augue in maximus vehicula. Pellentesque vestibulum, orci vel fringilla consectetur, diam eros cursus ligula, vel porta massa mauris vitae leo. Aenean blandit enim id fringilla rhoncus. Nullam vitae consectetur eros, eu bibendum justo. In laoreet dolor quis lectus porttitor, vitae porta nisi pellentesque.

Proin ac est ac mauris ullamcorper consectetur. Aliquam aliquet diam ultrices, pretium sem a, euismod metus. Quisque vitae libero id orci tristique convallis. Maecenas nec vulputate orci. Sed efficitur, mauris sit amet tincidunt pulvinar, ex elit tempus purus, ut malesuada sapien lorem at diam. Aliquam erat volutpat. Quisque ultrices tellus lacus, quis feugiat lacus dapibus quis. Nullam non arcu a urna tempor accumsan.`,
      replies: {},
      likes: {}
    },
    'bf52b698-014e-4ad7-9449-4207f21ad6d7': {
      id: 'bf52b698-014e-4ad7-9449-4207f21ad6d7',
      state: 'PUBLISHED',
      title: 'Mauris sed nisi',
      lastEditedAt: moment().subtract(_.random(11, 25), 'days').subtract(_.random(0, 24), 'hours').subtract(_.random(0, 60), 'minutes'),
      image: 'general/city',
      author: {
        id: '017e2721-d049-690f-4088-2e7a94f65884',
        name: 'Michael de Bie',
        email: 'mike@miketrala.com',
        roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
      },
      content: `Aliquam dictum vel ligula pellentesque consectetur. Nam lacinia fringilla turpis, a vehicula orci fermentum ac. Proin at elementum dui. Fusce libero tortor, blandit eu lectus a, semper sodales tellus. Donec ac venenatis lectus, id auctor dolor. Nunc interdum sagittis urna, fermentum fringilla dui congue et. Vivamus ipsum nulla, maximus eu orci eu, ullamcorper bibendum mi. Nunc bibendum orci a orci suscipit venenatis. Duis a fringilla urna, ut vehicula erat. Aliquam vestibulum ligula et fermentum elementum. Sed dignissim, metus et tristique euismod, lectus velit ultrices metus, non tincidunt nibh tellus ut diam. Integer id accumsan nulla. In odio odio, rutrum ut est at, tempor elementum lorem. Duis vel nisi semper turpis sagittis finibus nec sit amet neque. Etiam cursus pharetra lacus, non iaculis nunc condimentum porttitor.`,
      replies: {},
      likes: {}
    }
  }
})

export const getters = {
  get: state => id => {
    return state.list[id]
  },
  orderedList: state => () => {
    return _.orderBy(state.list, post => moment(post.lastEditedAt), ['desc'])
  },
  likes: () => subject => {
    return _.size(subject.likes)
  }
}

export const mutations = {
  like: (state, {subject, like}) => {
    Vue.set(subject.likes, like.id, like)
  },
  unlike: (state, {like, subject}) => {
    Vue.delete(subject.likes, like.id)
  },
  add: (state, post) => {
    Vue.set(state.list, post.id, post)
  },
  publish: (state, post) => {
    Vue.set(state.list, post.id, post)
  },
  edit: (state, {post, title, content, image, lastEditedAt}) => {
    post.title        = title
    post.content      = content
    post.image        = image
    post.lastEditedAt = lastEditedAt

    Vue.set(state.list, post.id, post)
  },
  remove: (state, {post}) => {
    Vue.delete(state.list, post.id)
  },
  addReply: (state, {parent, reply}) => {
    reply.lastEditedAt = null

    Vue.set(parent.replies, reply.id, reply)
  },
  editReply: (state, {parent, reply, content, lastEditedAt}) => {
    reply.content      = content
    reply.lastEditedAt = lastEditedAt

    Vue.set(parent.replies, reply.id, reply)
  },
  removeReply: (state, {parent, reply}) => {
    Vue.delete(parent.replies, reply.id)
  }
}

const delayedUuid = duration => {
  return new Promise(resolve => {
    setTimeout(() => {
      resolve(`7081dcda-6465-4f4c-b2ed-${_.random(100000, 999999)}`)
    }, duration || _.random(50, 200))
  })
}

export const actions = {
  async like({commit}, {subject, user}) {
    // async request be here
    const uuid = await delayedUuid()

    const like = {
      id: uuid,
      at: moment(),
      by: user
    }

    commit('like', {subject, like})
  },
  async unlike({commit, getter}, {subject, user}) {
    await delayedUuid()

    // noinspection JSCheckFunctionSignatures
    const like = _.find(subject.likes, like => like.by.id === user.id)

    if (null === like) {
      throw new Error('Like not found!')
    }

    commit('unlike', {subject, like})
  },
  async add({commit, rootGetters}, post) {
    // async request be here
    post.id           = await delayedUuid(2000)
    post.author       = rootGetters.currentUser()
    post.state        = 'DRAFT'
    post.lastEditedAt = moment()
    post.replies      = {}
    post.likes        = {}

    commit('add', post)
  },
  async publish({commit}, post) {
    // async request be here
    await delayedUuid()

    post.state        = 'PUBLISHED'
    post.lastEditedAt = moment()

    commit('publish', post)
  },
  async edit({commit, getter}, changes) {
    // async request be here
    await delayedUuid()

    changes.lastEditedAt = moment()

    commit('edit', changes)
  },
  async remove({commit, state, dispatch}, post) {
    const removeReplies = []

    _.each(post.replies, reply => {
      removeReplies.push(dispatch('removeReply', {
        reply,
        parent: post
      }))
    })

    await Promise.all(removeReplies)

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('remove', {post})
        resolve()
      }, _.random(50, 200))
    })
  },
  async addReply({commit, getters}, {parent, reply}) {
    // async request be here
    reply.id = await delayedUuid(1000)
    reply.at = moment()

    commit('addReply', {parent, reply})
  },
  async editReply({commit, getters}, {parent, reply, content}) {
    // async request be here
    await delayedUuid(1000)

    commit('editReply', {parent, reply, content, lastEditedAt: moment()})
  },
  async removeReply({commit, state, dispatch}, {parent, reply}) {
    const removeReplies = []

    _.each(reply.replies, child => {
      removeReplies.push(dispatch('removeReply', {
        parent: reply,
        reply: child
      }))
    })

    await Promise.all(removeReplies)

    return new Promise((resolve, reject) => {
      setTimeout(() => {
        commit('removeReply', {parent, reply})

        resolve()
      }, _.random(50, 200))
    })
  }
}
