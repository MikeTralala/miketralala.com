export const state = () => ({
  user: {
    id: '017e2721-d049-690f-4088-2e7a94f65884',
    name: 'Michael de Bie',
    email: 'mike@miketrala.com',
    roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
  },
  users: [
    {
      id: '017e2721-d049-690f-4088-2e7a94f65884',
      name: 'Michael de Bie',
      email: 'mike@miketrala.com',
      roles: ['ROLE_USER', 'ROLE_AUTHOR', 'ROLE_ADMINISTRATOR']
    },
    {
      id: '017e2721-d049-690f-3088-2e7a94f65821',
      name: 'Angela Sturm',
      email: 'angela@de_toet.nl',
      roles: ['ROLE_USER', 'ROLE_AUTHOR']
    },
    {
      id: '017e2721-d049-690f-4088-2e7a94f65821',
      name: 'John Doe',
      email: 'John@doe.com',
      roles: ['ROLE_USER']
    }
  ]
})

export const getters = {
  currentUser: state => () => {
    return state.user
  }
}

export const mutations = {
  logIn: () => {

  }
}
