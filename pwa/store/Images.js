export const state = () => ({
  'architecture': {
    'api-detail': '/architecture/api-detail.jpg',
    'flow': '/architecture/flow.jpg'
  },
  'data': {
    'data-center': '/data/data-center.jpg',
    'list': '/data/list.jpg',
    'matrix': '/data/matrix.jpg',
    'matrix-laptop': '/data/matrix-laptop.jpg'
  },
  'general': {
    'city': '/general/city.jpg',
    'colors-abstract': '/general/colors-abstract.jpg',
    'colors-liquid': '/general/colors-liquid.jpg',
    'colors-perspective': '/general/colors-perspective.jpg',
    'colors-rain': '/general/colors-rain.jpg',
    'colors-stack': '/general/colros-stack.jpg',
    'cubes': '/general/cubes.jpg'
  },
  'hardware': {
    'keyboard': '/hardware/keyboard.jpg',
    'print-board': '/hardware/print-board.jpg'
  },
  'one-liners': {
    'rewrite-edit': '/one-liners/rewrite-edit.jpg',
    'same-day-different-shit': '/one-liners/same-day-different-shit.jpg'
  },
  'programming': {
    'compiled-code-blurred': '/programming/compiled-code-blurred.jpg',
    'compiled-code-diagonal': '/programming/compiled-code-diagonal.jpg',
    'compiled-code-diagonal-alt1': '/programming/compiled-code-diagonal-alt1.jpg',
    'compiled-code-straight': '/programming/compiled-code-straight.jpg',
    'html': '/programming/html.jpg'
  },
  'security': {
    'general': '/security/general.jpg',
    'key': '/security/key.jpg',
    'settings': '/security/settings.jpg'
  },
  'error': {
    'error': '/error/error.jpg',
    'puzzle': '/error/puzzle.jpg',
    'thumb-down': '/error/thumb-down.jpg',
    'one-plus-one-equals-three': '/error/one-plus-one-equals-three.jpg'
  }
})

export const getters = {
  get: state => name => {
    const parts = name.split('/')

    if (2 !== parts.length) {
      throw new Error(`The image names should be formatted as "category/image", the provided name "${name}" is not`)
    }

    return state[parts[0]][parts[1]]
  },
  list: state => () => {

  }
}
