module.exports = {
  root: true,
  env: {
    browser: true,
    node: true
  },
  parserOptions: {
    parser: '@babel/eslint-parser',
    requireConfigFile: false
  },
  extends: [
    '@nuxtjs',
    'plugin:vue/base',
    'plugin:nuxt/recommended'
  ],
  plugins: [],
  // add your custom rules here
  rules: {
    'indent': 'off',
    'quote-props': 'off',
    'quotes': ['error', 'single', {'avoidEscape': true, 'allowTemplateLiterals': true}],
    'no-multi-spaces': 'off',
    'object-curly-spacing': ['error', 'never'],
    'no-trailing-spaces': ['error', {'skipBlankLines': true}],
    'space-before-function-paren': ['error', {'anonymous': 'always', 'named': 'never', 'asyncArrow': 'always'}],
    'operator-linebreak': ['error', 'before'],
    'yoda': ['error', 'always'],
    'arrow-parens': ['error', 'as-needed'],
    'space-unary-ops': ['error', {
      'words': true,
      'nonwords': false,
      'overrides': {
        '!': true
      }
    }],
    'vue/singleline-html-element-content-newline': ['error', {
      'ignoreWhenNoAttributes': true,
      'ignoreWhenEmpty': true,
      'ignores': ['pre', 'textarea', 'code', 'v-icon', 'span', 'svg', 'a', 'em']
    }]
  }
}
