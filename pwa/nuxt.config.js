import colors from 'vuetify/es5/util/colors'
import webpack from 'webpack'

export default {
  // Target: https://go.nuxtjs.dev/config-target
  target: 'server',

  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    titleTemplate: '%s | miketralala.com',
    title: 'miketralala.com',
    htmlAttrs: {
      lang: 'en'
    },
    meta: [
      {charset: 'utf-8'},
      {name: 'viewport', content: 'width=device-width, initial-scale=1'},
      {hid: 'description', name: 'description', content: ''},
      {name: 'format-detection', content: 'telephone=no'}
    ],
    link: [
      {rel: 'icon', type: 'image/x-icon', href: '/favicon.ico'}
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    {src: '~plugins/vue-particles', ssr: false}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: [
    // Equivalent to { path: '~/components' }
    '~/components'
  ],

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    '~/modules/idea-imports-fix.js',
    // https://go.nuxtjs.dev/eslint
    '@nuxtjs/eslint-module'
  ],
  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/vuetify
    '@nuxtjs/vuetify',
    '@nuxtjs/moment',
    '@nuxtjs/i18n'
  ],
  i18n: {
    strategy: 'prefix_except_default',
    langDir: 'lang/',
    defaultLocale: 'en',
    locales: [
      {code: 'nl', name: 'Nederlands', file: 'nl_NL.json'},
      {code: 'en', name: 'English', file: 'en_GB.json'}
    ],
    vueI18n: {
      defaultLocale: 'en',
      dateTimeFormats: {
        en: {
          short: {year: 'numeric', month: 'short', day: 'numeric'},
          long: {year: 'numeric', month: 'short', day: 'numeric', weekday: 'short', hour: 'numeric', minute: 'numeric'}
        },
        nl: {
          short: {year: 'numeric', month: 'short', day: 'numeric'},
          long: {year: 'numeric', month: 'short', day: 'numeric', weekday: 'long', hour: 'numeric', minute: 'numeric'}
        }
      }
    }
  },
  moment: {
    defaultLocale: 'en',
    locales: ['nl'],
    timezone: true,
    defaultTimezone: 'Europe/Amsterdam',
    matchZones: /Europe\/(Belfast|London|Paris|Athens|Amsterdam)/,
    startYear: 1970,
    endYear: 2050
  },
  // Vuetify module configuration: https://go.nuxtjs.dev/config-vuetify
  vuetify: {
    customVariables: ['~/assets/variables.scss'],
    theme: {
      dark: true,
      themes: {
        dark: {
          primary: colors.blueGrey.base,
          accent: colors.deepOrange.darken4,
          secondary: colors.deepOrange.darken2,
          info: colors.lightBlue.darken1,
          warning: colors.amber.base,
          error: colors.red.darken1,
          success: colors.green.accent1,
          action: colors.grey.darken3,
          edit: colors.lightBlue.darken1,
          delete: colors.red.darken2
        },
        light: {
          primary: colors.blue.darken2,
          accent: colors.grey.darken3,
          secondary: colors.amber.darken3,
          info: colors.teal.lighten1,
          warning: colors.amber.base,
          error: colors.deepOrange.accent4,
          success: colors.green.accent3,
          edit: colors.lightBlue.darken1,
          delete: colors.red.darken2
        }
      }
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
    plugins: [
      new webpack.ProvidePlugin({
        _: 'lodash'
      })
    ]
  }
}
