export const ConstraintBuilder = function (translator) {
  return {
    greaterThanOrEqual: (limit, subjectName = 'general.form.value') => {
      return data => (data || '').length >= limit
        || translator.t('error.form.greater-than-or-equal', {'subject': translator.t(subjectName), 'count': limit})
    },
    greaterThan: (limit, subjectName = 'general.form.value') => {
      return data => (data || '').length > limit
        || translator.t('error.form.greater-than', {'subject': translator.t(subjectName), 'count': limit})
    },

    lesserThanOrEqual: (limit, subjectName = 'general.form.value') => {
      return data => (data || '').length <= limit
        || translator.t('error.form.lesser-than-or-equal', {'subject': translator.t(subjectName), 'count': limit})
    },

    lesserThan: (limit, subjectName = 'general.form.value') => {
      return data => (data || '').length < limit
        || translator.t('error.form.lesser-than', {'subject': translator.t(subjectName), 'count': limit})
    }
  }
}
