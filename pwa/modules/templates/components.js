<%= options.getComponents().map(({ name, file }) => {
  return `import ${name} from '${file}'
import Lazy${name} from '${file}'`
}).join('\n') %>

<%= options.getComponents().map(({ name, file }) => {
  return `Vue.component('${name}', ${name})
Vue.component('Lazy${name}', ${name})`
}).join('\n') %>
