import {join, resolve} from 'path'
import glob from 'glob'

export default function ideaImportsFix() {
  if ('production' === process.env.NODE_ENV) {
    return
  }

  const components = glob.sync(join(__dirname, '../components/**/*.vue')).map(file => {
    const parts          = file.replace('.vue', '').split('/')
    const relevantParts  = parts.slice(parts.findIndex(value => 'components' === value) + 1)
    const name           = relevantParts.join('')

    return {name, file}
  })

  const getComponents = () => components

  this.addTemplate({
    src: resolve(__dirname, './templates', 'components.js'),
    fileName: '../.components.gen.js',
    options: {getComponents}
  })
}
