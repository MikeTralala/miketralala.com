export default {
  computed: {
    currentUser() {
      return this.$store.state.user
    },
    userIsSignedIn() {
      return null !== this.$store.state.user
    },
    userIsAdministrator() {
      return this.$store.state.user?.roles.includes('ROLE_ADMINISTRATOR')
    },
    userIsAuthor() {
      return this.$store.state.user?.roles.includes('ROLE_AUTHOR')
    }
  },
  methods: {
    isCurrentUser(object) {
      return this.$store.state.user?.id === object?.id
    },
    userIsAuthorOf(subject) {
      return this.isCurrentUser(subject?.author)
    }
  }
}
