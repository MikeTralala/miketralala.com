import _ from 'lodash'
import UsersMixins from './users.mixins'

export default {
  mixins: [UsersMixins],
  props: {
    tooltipPosition: {
      type: String,
      default: 'top',
      validator: position => ['top', 'right', 'bottom', 'left'].includes(_.toLower(position))
    },
    subject: {
      type: Object,
      required: true
    },
    disabled: {
      type: Boolean,
      default: false
    },
    inProgress: {
      type: Boolean,
      default: false
    },
    small: {
      type: Boolean,
      default: false
    },
    floatingActionButton: {
      type: Boolean,
      default: false
    },
    classes: {
      type: String,
      default: ''
    }
  },
  model: {
    prop: 'inProgress',
    event: 'updateInProgress'
  },
  computed: {
    isDisabled() {
      return this.disabled
        || ! this.userIsSignedIn
    },
    tooltipTop() {
      return this.tooltipIs('top')
    },
    tooltipRight() {
      return this.tooltipIs('right')
    },
    tooltipBottom() {
      return this.tooltipIs('bottom')
    },
    tooltipLeft() {
      return this.tooltipIs('left')
    }
  },
  methods: {
    tooltipIs(position) {
      return position === _.toLower(this.tooltipPosition)
    },
    setInProgress() {
      this.$emit('updateInProgress', true)
    },
    setIdle() {
      this.$emit('updateInProgress', false)
    }
  }
}
